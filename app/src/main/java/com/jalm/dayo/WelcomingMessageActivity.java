package com.jalm.dayo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WelcomingMessageActivity extends AppCompatActivity {

    double[] latLongOfUser = new double[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcoming_message);

        Intent intent = getIntent();

        latLongOfUser = intent.getDoubleArrayExtra(HomeActivity.intentLatLongOfUser);

        Button btnStart = (Button) findViewById(R.id.btnStart);


    }

    public void launchActivity(View view){

        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = SP.edit();
        editor.putBoolean("showWelcome", false).apply();


        Intent intent = new Intent(WelcomingMessageActivity.this, HomeActivity.class);
        intent.putExtra(HomeActivity.intentLatLongOfUser, latLongOfUser);
        startActivity(intent);

        finish();
    }
}
