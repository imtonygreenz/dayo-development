package com.jalm.dayo;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jalm.dayo.DataParser;
import com.jalm.dayo.DownloadURL;
import com.jalm.dayo.GetNearbyPlacesData;
import com.jalm.dayo.ListOfPlacesByCategoryActivity;
import com.jalm.dayo.PlaceDetailsActivity;
import com.jalm.dayo.forgsongetsearchresult.Result;
import com.jalm.dayo.forgsongetsearchresult.SearchResult;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tranc on 23/09/2017.
 */

public class GetNearbyPlacesDataForSearchResult extends AsyncTask<Object, Integer, List<HashMap<String,String>>>{

    private String googlePlacesData;
    private String url;
    private double [] latLongOfUser;
    private Activity activity;

    List<HashMap<String,String>> listOfPlaces;

    public GetNearbyPlacesDataForSearchResult(Activity activity){
        this.activity = activity;
    }

    protected List<HashMap<String,String>> doInBackground(Object... objects) {

        try{
            //googleMap = (GoogleMap)objects[0]; //passes the fragment and references the map from an activity
            url = (String)objects[0]; //passes a string object

            DownloadURL downloadURL = new DownloadURL();

            googlePlacesData = downloadURL.readURL(url);

            DataParser parse = new DataParser();
            listOfPlaces = parse.parse(googlePlacesData);


        } catch (IOException e){
            e.printStackTrace();
        }

            return listOfPlaces;
    }


}
