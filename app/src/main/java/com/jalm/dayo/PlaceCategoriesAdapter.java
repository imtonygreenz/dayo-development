package com.jalm.dayo;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by tranc on 10/09/2017.
 */

public class PlaceCategoriesAdapter extends ArrayAdapter<PlaceCategories> {

    PlaceCategories[] placeCategories;

    public PlaceCategoriesAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull PlaceCategories[] objects) {
        super(context, resource, objects);

        this.placeCategories = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v = convertView;

        if(v == null){
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.category_list_item, null);
        }

        ImageView ivIcon = (ImageView)v.findViewById(R.id.ivIcon);
        TextView tvCategory = (TextView)v.findViewById(R.id.tvCategoryName);

        PlaceCategories aSingleCategory = placeCategories[position];

        ivIcon.setImageResource(aSingleCategory.getPlaceCategoriesPicture());
        tvCategory.setText(aSingleCategory.getPlaceCategoriesName());

        return v;
    }
}
