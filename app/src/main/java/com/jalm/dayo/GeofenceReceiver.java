package com.jalm.dayo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class GeofenceReceiver extends BroadcastReceiver {

    Context context;


    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL) {

            Log.d("Geofence Transition","" + geofenceTransition);

            Log.d("Geofence", "I entered a geofence.");
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            String[] triggerIds = new String[triggeringGeofences.size()];

            for (int i = 0; i < triggerIds.length; i++) {
                triggerIds[i] = triggeringGeofences.get(i).getRequestId();
            }

            if(MyApplication.isActivityVisible()){
                Intent intent2 = new Intent("some_custom_id");
                intent2.putExtra(MapActivity.intentTRIGGEREDGEOFENCES, triggerIds);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent2);
            }else{
                Intent intent2 = new Intent("some_custom_id");
                intent2.putExtra(MapActivity.intentTRIGGEREDGEOFENCES, triggerIds);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent2);
            }
        }else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT){

            Log.d("Geofence", "I exited a geofence.");

            if(MyApplication.isActivityVisible()){
                Intent intent2 = new Intent("for_audio_walk");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent2);
            }else{
                Log.d("Geofence", "Broadcast not sent.");
                Intent intent2 = new Intent("for_audio_walk");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent2);
            }
        }
    }
}
