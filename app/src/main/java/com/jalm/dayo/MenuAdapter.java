package com.jalm.dayo;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by tranc on 28/09/2017.
 */

public class MenuAdapter extends ArrayAdapter<String>{

    String[] menuTitles = null;
    int[] iconsForMenu = null;

    Context context;

    String[] colors = new String[5];

    public MenuAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull String[] objects, int[] icons) {
        super(context, resource, objects);

        this.context = context;

        menuTitles = objects;

        colors[0] = context.getResources().getString(R.color.colorMenuItem2).substring(3);
        colors[1] = context.getResources().getString(R.color.colorMenuItem2).substring(3);
        colors[2] = context.getResources().getString(R.color.colorMenuItem2).substring(3);
        colors[3] = context.getResources().getString(R.color.colorMenuItem2).substring(3);
        colors[4] = context.getResources().getString(R.color.colorMenuItem2).substring(3);

        iconsForMenu = icons;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.drawer_list_item, null);

        TextView textView = (TextView)v.findViewById(R.id.tvMenuItem);
        textView.setBackgroundColor(Color.parseColor("#" + colors[position]));

        ImageView ivMenuItemIcon = (ImageView)v.findViewById(R.id.ivMenuItemIcon);
        ivMenuItemIcon.setBackgroundColor(Color.parseColor("#" + colors[position]));
        ivMenuItemIcon.setImageResource(iconsForMenu[position]);

        textView.setText(menuTitles[position]);

        return v;
    }

}
