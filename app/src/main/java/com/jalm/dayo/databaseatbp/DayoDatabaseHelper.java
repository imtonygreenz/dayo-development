package com.jalm.dayo.databaseatbp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jalm.dayo.R;

/**
 * Created by tranc on 10/09/2017.
 */

public class DayoDatabaseHelper extends SQLiteOpenHelper{

    public static final String TB_PP = "POPULARPLACES";
    public static final String TB_PJ = "PICTUREJOIN";
    public static final String TB_PIC = "PICTURE";
    public static final String TB_EA = "ENGLISHAUDIO";
    public static final String TB_TA = "TAGALOGAUDIO";
    public static final String TB_EMA = "ENGLISHMISCAUDIO";
    public static final String TB_TMA = "TAGALOGMISCAUDIO";

    private static final String DB_NAME = "dayo";
    private static final int DB_VERSION = 5; //you have to clear the data of the app first in order for this to work without error when changing this

    Context myContext;

    public DayoDatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
        myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        updateMyDatabase(sqLiteDatabase, 0, DB_VERSION);
        Log.d("Database: ", "Database initially created.");
    }

    //will be invoked if DB from device is older than the DB_VERSION specified above
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //hindi l yung parameter, it is a one, i one
        updateMyDatabase(sqLiteDatabase, i, i1);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //myContext.deleteDatabase(DB_NAME);
    }

    public static void insertPopularPlace(SQLiteDatabase sqLiteDatabase, String name, String address,
                                          String description, double latitude, double longitude){
        ContentValues placeValues = new ContentValues();
        placeValues.put("NAME", name);
        placeValues.put("ADDRESS", address);
        placeValues.put("DESCRIPTION", description);
        placeValues.put("LATITUDE", latitude);
        placeValues.put("LONGITUDE", longitude);

        sqLiteDatabase.insert("POPULARPLACES", null, placeValues);
    }

    private void updateMyDatabase(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion){

        Log.d("Database: ", "oldVersion = " + oldVersion);
        Log.d("Database: ", "newVersion = " + newVersion);

        if(oldVersion < 1){
            StringBuilder query = new StringBuilder();

            query.append("CREATE TABLE POPULARPLACES (");
            query.append("_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
            query.append("NAME TEXT, ");
            query.append("ADDRESS TEXT, ");
            query.append("DESCRIPTION TEXT, ");
            query.append("LATITUDE REAL, ");
            query.append("LONGITUDE REAL);");

            sqLiteDatabase.execSQL(query.toString());

            insertPopularPlace(sqLiteDatabase, "Perez Park", "Lucena, 4301 Quezon", "Ang Perez Park " +
                    "ay isa sa pinakamalaking park dito sa Lungsod ng Lucena. " +
                    "Kilala ito dito bilang pasyalan ng mga pamilya at barkada. " +
                    "Pumupunta rin ang mga kabataan dito para magpractice ng kanilang " +
                    "mga sayaw para sa eskwela o kompetisyon. ", 13.9287437, 121.6129413);
//            insertPopularPlace(sqLiteDatabase, "RC Burger", "El Merchanto, Merchan Street, Barangay 9, " +
//                    "Lucena, 4301 Quezon Province", "Ang RC Burger ay may budget burgers na swak na swak" +
//                    " sa budget mo. Kahit mura, masarap pa rin!", 13.9302175, 121.6140124);
            insertPopularPlace(sqLiteDatabase, "Quezon Provincial Capitol", "230 Merchan St, Lucena, 4301 Quezon",
                    "Isa sa pinaka lumang building na meron sa Lucena ito na ang Tayabas Capitol. " +
                            "Dito din matatagpuan mo ang office ng Post office, DWD, Civil dayoService " +
                            "office at iba pa.", 13.928404,121.613229);
            insertPopularPlace(sqLiteDatabase, "Quezon Convention Center", "Lucena, 4301 Quezon",
                    "Ang nakikita mong malaking gusali ay ang Quezon Convention Center. " +
                            "Dito ginaganap ang mga malalaking event tulad ng beauty pageants, concert o" +
                            " PBA games.", 13.9284759,121.6138693);
            insertPopularPlace(sqLiteDatabase, "STI Academic Center Lucena", "Quezon Avenue Corner Perez Street",
                    "Education for real life. Isa ito sa mga kilalang colleges dito sa Lucena. Isa sila" +
                            "sa may mga magagandang facilities dito sa Lucena.", 13.929811, 121.612756);

            

            Log.d("Database: ", "PP table initially created.");
        }


        //this doesn't work at first because, onUpgrade isn't invoke, db on the device is the same as the db on the phone
        if(oldVersion < 2){
            StringBuilder query = new StringBuilder();

            query.append("CREATE TABLE PICTURE (");
            query.append("PICID INTEGER PRIMARY KEY AUTOINCREMENT, ");
            query.append("RESOURCEID INTEGER); ");

            sqLiteDatabase.execSQL(query.toString());//the table is created
            Log.d("Database: ", "Picture table created.");

            query = new StringBuilder();//resets the contents of query

            //creation of the joining table for popular places and picture
            query.append("CREATE TABLE PICTUREJOIN (");
            query.append("PPID INTEGER, "); //foreign key
            query.append("PICID INTEGER);"); //foreign key

            sqLiteDatabase.execSQL(query.toString());
            Log.d("Database: ", "PictureJoin table created.");

            insertPicture(sqLiteDatabase, R.drawable.pp_perezpark);
            insertPicture(sqLiteDatabase, R.drawable.pp_perezpark1);
            //insertPicture(sqLiteDatabase, R.drawable.pp_perezpark2);
//            insertPicture(sqLiteDatabase, R.drawable.pp_rcburger);
//            insertPicture(sqLiteDatabase, R.drawable.pp_rcburger1);
            insertPicture(sqLiteDatabase, R.drawable.pp_quezonprovincialcapitol);
            insertPicture(sqLiteDatabase, R.drawable.pp_quezonprovincialcapitol1);
            insertPicture(sqLiteDatabase, R.drawable.pp_quezonprovincialcapitol2);
            insertPicture(sqLiteDatabase, R.drawable.pp_quezonprovincialcapitol3);
            insertPicture(sqLiteDatabase, R.drawable.pp_quezonconventioncenter);
            //insertPicture(sqLiteDatabase, R.drawable.pp_quezonconventioncenter1);
            insertPicture(sqLiteDatabase, R.drawable.pp_stiacademiccenterlucena);


            //1 - Perez Park

            //2 - Quezon Provincial Capitol
            //3 - Quezon Convention Center
            //4 - STI Academic Center Lucena

            insertPictureJoin(sqLiteDatabase, 1, 1);
            insertPictureJoin(sqLiteDatabase, 1, 2);
            //insertPictureJoin(sqLiteDatabase, 1, 3);
//            insertPictureJoin(sqLiteDatabase, 2, 4);
//            insertPictureJoin(sqLiteDatabase, 2, 5);
            insertPictureJoin(sqLiteDatabase, 2, 3);
            insertPictureJoin(sqLiteDatabase, 2, 4);
            insertPictureJoin(sqLiteDatabase, 2, 5);
            insertPictureJoin(sqLiteDatabase, 2, 6);
            insertPictureJoin(sqLiteDatabase, 3, 7);
            //insertPictureJoin(sqLiteDatabase, 3, 9);
            insertPictureJoin(sqLiteDatabase, 4, 8);
        }

        if(oldVersion < 3){
            StringBuilder query = new StringBuilder();

            query.append("CREATE TABLE TAGALOGAUDIO (");
            query.append("AID INTEGER PRIMARY KEY AUTOINCREMENT, ");
            query.append("RESOURCEID INTEGER); ");

            sqLiteDatabase.execSQL(query.toString());//the table is created

            query = new StringBuilder();

            query.append("CREATE TABLE ENGLISHAUDIO (");
            query.append("AID INTEGER PRIMARY KEY AUTOINCREMENT, ");
            query.append("RESOURCEID INTEGER); ");

            sqLiteDatabase.execSQL(query.toString());//the table is created

            //1 - Perez Park
            //2 - RC Burger
            //3 - Quezon Provincial Capitol
            //4 - Quezon Convention Center

            insertTagalogAudio(sqLiteDatabase,R.raw.aud_tag_perezpark);
//            insertTagalogAudio(sqLiteDatabase,R.raw.aud_tag_rcburger);
            insertTagalogAudio(sqLiteDatabase,R.raw.aud_tag_capitol);
            insertTagalogAudio(sqLiteDatabase,R.raw.aud_tag_quezonconventioncenter);
            insertTagalogAudio(sqLiteDatabase,R.raw.aud_tag_sticollegelucena);

            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_perezpark);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_capitol);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_quezonconventioncenter);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_sticollegelucena);
        }

        if(oldVersion < 4){
            StringBuilder query = new StringBuilder();

            query.append("CREATE TABLE TAGALOGMISCAUDIO (");
            query.append("AID INTEGER PRIMARY KEY AUTOINCREMENT, ");
            query.append("RESOURCEID INTEGER); ");

            sqLiteDatabase.execSQL(query.toString());//the table is created

            query = new StringBuilder();

            query.append("CREATE TABLE ENGLISHMISCAUDIO (");
            query.append("AID INTEGER PRIMARY KEY AUTOINCREMENT, ");
            query.append("RESOURCEID INTEGER); ");

            sqLiteDatabase.execSQL(query.toString());//the table is created

            insertTagalogMiscAudio(sqLiteDatabase, R.raw.aud_tag_audiowalk);
            insertTagalogMiscAudio(sqLiteDatabase, R.raw.aud_tag_audiowalk2);
            insertTagalogMiscAudio(sqLiteDatabase, R.raw.aud_tag_audiowalk3);
            insertTagalogMiscAudio(sqLiteDatabase, R.raw.aud_tag_audiowalk4);

            insertEnglishMiscAudio(sqLiteDatabase, R.raw.aud_eng_audiowalk);
            insertEnglishMiscAudio(sqLiteDatabase, R.raw.aud_eng_audiowalk2);
        }

        if(oldVersion < 5){
            insertPopularPlace(sqLiteDatabase, "Bubbles", "Granja St, Corner Lakandula", "Kung Crispy Pata ang hanap mo, dito sa Bubbles ang marerekomenda ko. Dito ang pinakakilalang Crispy Pata sa Lucena.", 13.929621,121.612259);
            insertPopularPlace(sqLiteDatabase, "Governor's Mansion", "Lucena, 4301 Quezon", "Isa ito sa lumang style ng bahay na iyong makikita sa Lucena City na ngayon na siyang nagsisilibing office ng aming mahal na Governador.",13.927955, 121.611951);
            insertPopularPlace(sqLiteDatabase, "Quezon Monument", "Lucena, 4301 Quezon", "Nasa monumento ka ng dating Pangulong Manuel Luis Quezon ngayon. At tuwing araw ng kanyang kapanganakan dito nag aalay ng bulaklak ang mga nasa Government office upang magbigay pugay sa dating Pangulo.",13.927570, 121.613032 );
            insertPopularPlace(sqLiteDatabase, "Tennis Court", "Lucena 4301 Quezon", "Ito ang isa sa mga tennis court dito sa Lucena. Dito kadalasang nagtitipon ang mga tennis players ng Lucena upang makipaglaro. Pwede ka ring makilaro!" ,13.929304, 121.614166);
            insertPopularPlace(sqLiteDatabase, "Philippine Tong Ho Institute", "Lucena 4301 Quezon", "Ang Philippine Tong-Ho Institute o Tong-Ho ay isang Chinese high school. Itinayo to noong 1921.",  13.929911, 121.613714 );
            insertPopularPlace(sqLiteDatabase, "Ocean Palace Mall", "Ocean Palace Mall, Granja St, Lucena, 4301 Quezon", "This building used to be a mall. Specifically, the first mall here in Lucena City. OPM stands for Ocean Palace Mall. Sadly, it is now abandoned.", 13.932132, 121.612349);
            insertPopularPlace(sqLiteDatabase, "Granja Panciteria", "Granja Street corner Leon Guinto Street, Barangay 8, Lucena, 4301 Quezon", "Ito ang Granja Panciteria, isa sa pinakalumang panciteria sa Lucena. Kilala ito sa kanilang iba’t ibang uring pancit na kadalasang linalagay nila sa bilao.", 13.931292, 121.612010);
            insertPopularPlace(sqLiteDatabase, "Orelianne's Thai Kitchen", "Merchan St, Barangay 9, Lucena, 4301 Quezon Province", "Ang Orelianne’s Thai Kitchen ay isang restaurant na may thai food. Meron din silang mga budget meals para sa mga estudyante.", 13.930415, 121.613667);

            //5 Bubbles
            //6 Gov. Mansion
            //7 Quezon Monument
            //8 Tennis Court
            //9 Tong Ho

            //10 OPM
            //11 Granja Pancit
            //12 Orelianne


                                                                //PICID
            insertPicture(sqLiteDatabase, R.drawable.pp_bubbles); //11
            insertPicture(sqLiteDatabase, R.drawable.pp_bubbles2); //12
            insertPicture(sqLiteDatabase, R.drawable.pp_governorsmansion); //13
            insertPicture(sqLiteDatabase, R.drawable.pp_quezonmonument); //14
            insertPicture(sqLiteDatabase, R.drawable.pp_quezonmonument2); //15
            insertPicture(sqLiteDatabase, R.drawable.pp_tenniscourt); //16
            insertPicture(sqLiteDatabase, R.drawable.pp_tongho); //17

            insertPicture(sqLiteDatabase, R.drawable.pp_opm); //18
            insertPicture(sqLiteDatabase, R.drawable.pp_granjapanciteria); //19
            insertPicture(sqLiteDatabase, R.drawable.pp_granjapanciteria2); //20
            insertPicture(sqLiteDatabase, R.drawable.pp_oreliannesthaikitchen); //21



            insertPictureJoin(sqLiteDatabase, 5, 9);
            insertPictureJoin(sqLiteDatabase, 5, 10);
            insertPictureJoin(sqLiteDatabase, 6, 11);
            insertPictureJoin(sqLiteDatabase, 7, 12);
            insertPictureJoin(sqLiteDatabase, 7, 13);
            insertPictureJoin(sqLiteDatabase, 8, 14);
            insertPictureJoin(sqLiteDatabase, 9, 15);
            insertPictureJoin(sqLiteDatabase, 10, 16);
            insertPictureJoin(sqLiteDatabase, 11, 17);
            insertPictureJoin(sqLiteDatabase, 11, 18);
            insertPictureJoin(sqLiteDatabase, 12, 19);



            insertTagalogAudio(sqLiteDatabase, R.raw.aud_tag_bubbles);
            insertTagalogAudio(sqLiteDatabase, R.raw.aud_tag_governorsmansion);
            insertTagalogAudio(sqLiteDatabase, R.raw.aud_tag_monument);
            insertTagalogAudio(sqLiteDatabase, R.raw.aud_tag_tenniscourt);
            insertTagalogAudio(sqLiteDatabase, R.raw.aud_tag_tongho);
            insertTagalogAudio(sqLiteDatabase, 0);
            insertTagalogAudio(sqLiteDatabase, R.raw.aud_tag_granjapanciteria);
            insertTagalogAudio(sqLiteDatabase, R.raw.aud_tag_otk);


            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_bubbles);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_governorsmansion);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_monument);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_tenniscourt);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_tongho);
            insertEnglishAudio(sqLiteDatabase, R.raw.aud_eng_opm);
            insertEnglishAudio(sqLiteDatabase, 0);
            insertEnglishAudio(sqLiteDatabase, 0);
        }


    }

    //inserting a picture within the picture table of the dayo database
    public static void insertPicture(SQLiteDatabase sqLiteDatabase, int resourceId){
        ContentValues pictureValues = new ContentValues();
        pictureValues.put("RESOURCEID", resourceId);

        sqLiteDatabase.insert("PICTURE", null, pictureValues);
    }

    //inserting joining values within the picturejoin table of the dayo database
    public static void insertPictureJoin(SQLiteDatabase sqLiteDatabase, int popularPlaceId, int pictureId){
        ContentValues pictureJoinValues = new ContentValues();
        pictureJoinValues.put("PPID", popularPlaceId);
        pictureJoinValues.put("PICID", pictureId);

        sqLiteDatabase.insert("PICTUREJOIN", null, pictureJoinValues);
    }

    public static void insertTagalogAudio(SQLiteDatabase sqLiteDatabase, int audioId){
        ContentValues tagalogAudioValues = new ContentValues();
        tagalogAudioValues.put("RESOURCEID", audioId);

        sqLiteDatabase.insert("TAGALOGAUDIO", null, tagalogAudioValues);
    }

    public static void insertEnglishAudio(SQLiteDatabase sqLiteDatabase, int audioId){
        ContentValues englishAudioValues = new ContentValues();
        englishAudioValues.put("RESOURCEID", audioId);

        sqLiteDatabase.insert("ENGLISHAUDIO", null, englishAudioValues);
    }

    public static void insertTagalogMiscAudio(SQLiteDatabase sqLiteDatabase, int audioId){
        ContentValues tagalogAudioValues = new ContentValues();
        tagalogAudioValues.put("RESOURCEID", audioId);

        sqLiteDatabase.insert("TAGALOGMISCAUDIO", null, tagalogAudioValues);
    }

    public static void insertEnglishMiscAudio(SQLiteDatabase sqLiteDatabase, int audioId){
        ContentValues tagalogAudioValues = new ContentValues();
        tagalogAudioValues.put("RESOURCEID", audioId);

        sqLiteDatabase.insert("ENGLISHMISCAUDIO", null, tagalogAudioValues);
    }

}
