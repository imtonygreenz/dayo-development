package com.jalm.dayo;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Acer on 17 Sep 2017.
 */

public class eventsAdapter extends BaseExpandableListAdapter {

    private Context c;
    private ArrayList<eventsDetails> eventsDetails;
    private LayoutInflater inflater;

    public eventsAdapter(Context c, ArrayList<eventsDetails> eventsDetails) {
        this.c = c;
        this.eventsDetails = eventsDetails;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    //get number of events
    @Override
    public int getGroupCount() {
        return eventsDetails.size();
    }
    //get number of child
    @Override
    public int getChildrenCount(int groupPosition) {
        return eventsDetails.get(groupPosition).eventnames.size();
    }
    //get events
    @Override
    public Object getGroup(int groupPosition) {
        return eventsDetails.get(groupPosition);
    }
    // return a detail
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return eventsDetails.get(groupPosition).eventnames.get(childPosition);
    }
    //get event id
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }
    //get event id
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
    //get event row
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if(convertView==null)
            {
                convertView=inflater.inflate(R.layout.eventspic, null);
            }

            //get event item
            eventsDetails t=(eventsDetails) getGroup(groupPosition);

            //set group name
            TextView eventDetail = (TextView) convertView.findViewById(R.id.eventDetailsText);
            ImageView img = (ImageView) convertView.findViewById(R.id.imageViewEvents);

            String name = t.eventName;
            //eventDetail.setText(name);

            //assign image accdg to name
        if (name == "Niyogyugan Festival") {
            img.setImageResource(R.drawable.niyogyugan);
        } else if (name == "Chami Festival") {
            img.setImageResource(R.drawable.chamifestival1);
        } else if (name == "Quezon Day") {
            img.setImageResource(R.drawable.quezonday1);
        } else if (name == "Araw ng Lucena") {
            img.setImageResource(R.drawable.lucenaday);
        } else if (name == "Hagisan ng Suman") {
            img.setImageResource(R.drawable.suman);
        } else if (name == "Pasayahan sa Lucena") {
            img.setImageResource(R.drawable.pasayahan);
        } else if (name == "Hermano Pule Day") {
            img.setImageResource(R.drawable.hermanopule);
        }

        //set bg
        convertView.setBackgroundColor(Color.TRANSPARENT);


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.eventsdetails, null);
        }
        //get event name
        String eventn = (String) getChild(groupPosition, childPosition);

        //set event details
        TextView eventDetail = (TextView) convertView.findViewById(R.id.eventDetailsText);
        // ImageView img = (ImageView) convertView.findViewById(R.id.imageViewEvents);

        eventDetail.setText(eventn);

            //get eventname
            String eventname = getGroup(groupPosition).toString();

            if (eventname == "Niyogyugan Festival") {
                eventDetail.setText(eventn);
            } else if (eventname == "Chami Festival") {
                eventDetail.setText(eventn);
            } else if (eventname == "Quezon Day") {
                eventDetail.setText(eventn);
            } else if (eventname == "Araw ng Lucena") {
                eventDetail.setText(eventn);
            } else if (eventname == "Hagisan ng Suman") {
                eventDetail.setText(eventn);
            } else if (eventname == "Pasayahan sa Lucena") {
                eventDetail.setText(eventn);
            } else if (eventname == "Hermano Pule Day") {
                eventDetail.setText(eventn);
            }
            eventDetail.setTextColor(Color.WHITE);
            eventDetail.setPadding(15, 15, 15, 15);
            eventDetail.setGravity(Gravity.RIGHT);
            eventDetail.setGravity(Gravity.LEFT);
            convertView.setBackgroundColor(Color.DKGRAY);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
