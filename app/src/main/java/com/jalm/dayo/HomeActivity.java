package com.jalm.dayo;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.SharedPreferencesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class HomeActivity extends AppCompatActivity {

    public static final String intentLatLongOfUser = "forCoordinates";

    private String[] drawerMenuTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    private ActionBarDrawerToggle mDrawerToggle;

    private double latitude;
    private double longitude;

    private double[] latLongOfUser = new double[2]; //fetched coordinates of the user

    private Bundle appData;

    ListView lvPopularPlaces;

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        Calendar calendar = Calendar.getInstance();
//        int dayofmonth = calendar.get(Calendar.DAY_OF_MONTH);
//        int month = calendar.get(Calendar.MONTH);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(this);
        boolean playAudio = SP.getBoolean("promptStartup", true);

        if(playAudio){
            playWelcomeAudio();
        }




        Intent intent = getIntent();

        if(intent.hasExtra(intentLatLongOfUser)){
            latLongOfUser = intent.getDoubleArrayExtra(intentLatLongOfUser);
            appData = new Bundle();
            appData.putDoubleArray(ListOfPlacesByCategoryActivity.intentLATLONG,latLongOfUser);
        }else{
            getCoordinates();
        }

        eventsNotification();


//        switch (month) {
//            case 7:
//                if (dayofmonth == 17){
//                    eventNotification("Ongoing big event in Lucena City!", "Lucena City is celebrating Niyogyugan Festival!");
//                    break;
//                }
//                else if (dayofmonth == 7){
//                    eventNotification("Today is Lucena City Day!", "Lucena City is celebrating Lucena Day!");
//                    break;
//                }
//                else if (dayofmonth == 19){
//                    eventNotification("Happy Birthday Manuel L. Quezon!", "Lucena City is celebrating Quezon Day!");
//                    break;
//                }
//                else {
//                    break;
//                }
//            case 10:
//                if (dayofmonth == 4){
//                    eventNotification("Today is a special day.", "Lucena City is giving respects to Hermano Puli, their local hero.");
//                }
//                else {
//                    break;
//                }
//            case 4:
//                if (dayofmonth ==  15){
//                    eventNotification("Suman Party!", "Lucena City is celebrating Hagisan ng Suman!");
//                    break;
//                }
//                else if (dayofmonth == 26){
//                    eventNotification("Chami Galore!", "Lucena City is celebrating Chami Festival! YUMM!");
//                    break;
//                }
//                else if (dayofmonth == 24){
//                    eventNotification("Pasayahan in Lucena!", "Lucena City is celebrating Pasayahan! Come and celebrate!");
//                    break;
//                }
//                else {
//                    break;
//                }
//
//
//        }


        //references the layout items
        drawerMenuTitles = getResources().getStringArray(R.array.drawer_menu_titles);
        int[] icons = {R.drawable.ic_dns_white_24dp, R.drawable.ic_event_white_24dp, R.drawable.ic_explore_white_24dp,
                        R.drawable.ic_group_work_white_24dp, R.drawable.ic_settings_white_24dp};

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };


        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        mDrawerList.setAdapter(new MenuAdapter(this, R.layout.drawer_list_item, drawerMenuTitles,icons));
        mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                View parent = (View) view.getParent();


                //TextView aSingleMenu = (TextView)parent.findViewById(R.id.tvMenuItem);
                //String menuName = "" + aSingleMenu.getText();
                selectItem(l + "");

                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        lvPopularPlaces = (ListView) findViewById(R.id.lvPopularPlaces);


        GetPopularPlacesData getPopularPlacesData = new GetPopularPlacesData(this);
        getPopularPlacesData.execute();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.homeMenu_place_search:
                onSearchRequested();
                break;
            case android.R.id.home:
                if(mDrawerLayout.isDrawerOpen(Gravity.START)){
                    mDrawerLayout.closeDrawers();
                }else{
                    mDrawerLayout.openDrawer(Gravity.START);
                }
        }

        return super.onOptionsItemSelected(item);
    }

    private void selectItem(String menuCount) {

        Intent intent;
        switch (menuCount) {
            case "3":
                intent = new Intent(getApplicationContext(), AboutUsActivity.class);
                startActivity(intent);
                break;
            case "0":
                intent = new Intent(getApplicationContext(), PlaceCategoriesActivity.class);
                startActivity(intent);
                break;
            case "1":
                intent = new Intent(getApplicationContext(), EventsActivity.class);
                startActivity(intent);
                break;
            case "2":
                intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra(MapActivity.intentINITLATLONG,latLongOfUser );
                startActivity(intent);
                break;
            case "4":
                intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                break;

        }
    }

    public void settingTheAdapter(Cursor cursor) {
        lvPopularPlaces.setAdapter(new PopularPlacesAdapter(this, cursor));
        lvPopularPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(HomeActivity.this, "" + l + " selected", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(HomeActivity.this, PopularPlacesDetailsActivity.class);

                //variable l is PopularPlacesAdapter .getItemId value
                intent.putExtra(PopularPlacesDetailsActivity.intentASINGLEPOPULARPLACE, l);
                intent.putExtra(PopularPlacesDetailsActivity.intentLATLONGOFUSER,latLongOfUser);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSearchRequested() {
        startSearch(null, false, appData, false);
        return true;
    }


    //inflates the menu item (including the icon) to the app bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void getCoordinates() {
        FusedLocationProviderClient mFusedLocationClient = mFusedLocationClient = getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {


                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    //Toast.makeText(HomeActivity.this, "Location fetch success.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(HomeActivity.this, "Lat: " + latitude + " Long: + " + longitude, Toast.LENGTH_SHORT).show();

                    latLongOfUser[0] = latitude;
                    latLongOfUser[1] = longitude;

                    appData = new Bundle();
                    appData.putDoubleArray(ListOfPlacesByCategoryActivity.intentLATLONG,latLongOfUser);
                } else {
                    //Toast.makeText(HomeActivity.this, "Location fetch failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

//    private void eventNotification(String title, String message) {
//        NotificationManager notificationManager;
//        int notifID=33;
//
//        NotificationCompat.Builder notificBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
//                        .setContentTitle(title)
//                        .setContentText(message)
//                        .setTicker("Event in Lucena City!")
//                        .setSmallIcon(R.drawable.cast_ic_notification_small_icon);
//        notificBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
//
//        Intent eventIntent = new Intent(this, EventsActivity.class);
//
//        TaskStackBuilder tStackBuilder = TaskStackBuilder.create(this);
//
//        tStackBuilder.addParentStack(EventsActivity.class);
//        tStackBuilder.addNextIntent(eventIntent);
//
//        PendingIntent pendingIntent = tStackBuilder.getPendingIntent(17, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        notificBuilder.setContentIntent(pendingIntent);
//
//        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(notifID, notificBuilder.build());
//
//
//
//
//    }

    public void eventsNotification(View view){

        Long alertTime = new GregorianCalendar().getTimeInMillis()+1*1000;

        Intent eventNotifIntent = new Intent(this, AlertReceiver.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alertTime, PendingIntent.getBroadcast(this, 17, eventNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT));

    }

    public void eventsNotification(){

        Long alertTime = new GregorianCalendar().getTimeInMillis()+1*1000;

        Intent eventNotifIntent = new Intent(this, AlertReceiver.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alertTime, PendingIntent.getBroadcast(this, 18, eventNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT));

    }

    public void timeNotification(View view) {

        Long aTime = new GregorianCalendar().getTimeInMillis()+2*1000;

        Intent timeNotifIntent = new Intent(this, AlertReceiver.class);
        AlarmManager tAlManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        tAlManager.set(AlarmManager.RTC_WAKEUP, aTime, PendingIntent.getBroadcast(this, 18, timeNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT));

    }
    public void timeNotification() {

        Long aTime = new GregorianCalendar().getTimeInMillis()+2*1000;

        Intent timeNotifIntent = new Intent(this, AlertReceiver.class);
        AlarmManager tAlManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        tAlManager.set(AlarmManager.RTC_WAKEUP, aTime, PendingIntent.getBroadcast(this, 18, timeNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT));

    }

    private void playWelcomeAudio(){

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String selectedTourVoice = sp.getString("selectedTourVoice","1");

        int welcomeMessage = 0;

        if(selectedTourVoice.equals("1")){
            welcomeMessage = R.raw.aud_tag_welcomemessage;
        }else if(selectedTourVoice.equals("2")){
            welcomeMessage = R.raw.aud_eng_welcomemessage;
        }

        mediaPlayer = MediaPlayer.create(this, welcomeMessage);
        mediaPlayer.start();

    }

    public void onBackPressed() {
        super.onBackPressed();
        if(mediaPlayer != null){
            if(mediaPlayer.isPlaying()){
                mediaPlayer.stop();
                mediaPlayer.release();
            }
        }

    }


}