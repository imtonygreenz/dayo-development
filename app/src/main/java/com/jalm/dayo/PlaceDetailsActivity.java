package com.jalm.dayo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.*;
import android.provider.Contacts;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jalm.dayo.forgsongetdetails.*;
import com.jalm.dayo.forgsongetdetails.Location;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

//TODO: FIx animation bug


public class PlaceDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String intentASINGLEPLACE = "aSinglePlace";
    public static final String intentUSERLATLONG = "userLatLong";

    private HashMap<String, String> aSinglePlace;
    private LatLng latLongOfAPlace;
    private double[] latLongOfUser;
    private LatLng latLongOfDevice;

    private LinearLayout layForPic2;

    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;

    private GoogleMap mGoogleMap;

    private Marker deviceLocation;
    private Marker destinationLocation;
    private Polyline polyline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details);

        Intent intent = getIntent();
        aSinglePlace = (HashMap<String, String>) intent.getSerializableExtra(intentASINGLEPLACE);
        latLongOfUser = intent.getDoubleArrayExtra(intentUSERLATLONG);

        TextView tvPlaceName = (TextView) findViewById(R.id.tvPlaceName);

        //Prompts
        final TextView lblPhoneNumber = (TextView) findViewById(R.id.lblPhoneNumber);
        final TextView lblInternationalPhoneNumber = (TextView) findViewById(R.id.lblInternationalPhoneNumber);
        final TextView lblWebsite = (TextView) findViewById(R.id.lblWebsite);

        final ImageView bulletPhone = (ImageView)findViewById(R.id.bulletPhone);
        final ImageView bulletPhone2 = (ImageView)findViewById(R.id.bulletPhone2);
        final ImageView bulletWebsite = (ImageView)findViewById(R.id.bulletWebsite);
        layForPic2 = (LinearLayout) findViewById(R.id.layForPic2);

        lblPhoneNumber.setVisibility(View.INVISIBLE);
        lblInternationalPhoneNumber.setVisibility(View.INVISIBLE);
        lblWebsite.setVisibility(View.INVISIBLE);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().
                findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tvPlaceName.setText(aSinglePlace.get("place_name"));
        final Details details;
        GetDetailsData getDetailsData = new GetDetailsData(this);
        try {
            details = getDetailsData.execute(aSinglePlace.get("reference")).get(); //this freezes the ui btw


            /*Referencing the views in the layout*/
            final TextView tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
            final TextView tvInternationalPhoneNumber = (TextView) findViewById(R.id.tvInternationalPhoneNumber);
            final TextView tvWebsite = (TextView) findViewById(R.id.tvWebsite);

            final ScrollView sv_PlaceDetailsScroller = (ScrollView)
                    findViewById(R.id.sv_placeDetailsScroller);

            ImageView transparentImage2 = (ImageView) findViewById(R.id.transparent_image2);

            transparentImage2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    int action = motionEvent.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            sv_PlaceDetailsScroller.requestDisallowInterceptTouchEvent(true);
                            Log.d("ONTOUCH: ", "Down");
                            return false;
                        case MotionEvent.ACTION_UP:
                            sv_PlaceDetailsScroller.requestDisallowInterceptTouchEvent(false);
                            Log.d("ONTOUCH: ", "Up");
                            return true;
                        case MotionEvent.ACTION_MOVE:
                            sv_PlaceDetailsScroller.requestDisallowInterceptTouchEvent(true);
                            Log.d("ONTOUCH: ", "Move");
                            return false;
                        default:
                            return true;
                    }
                }
            });


            Result result = details.getResult();


            if (result.getFormattedAddress() != null) {
                TextView tvAddress = (TextView) findViewById(R.id.tvAddress);
                tvAddress.setText(result.getFormattedAddress());
            }


            if (result.getFormattedPhoneNumber() != null) {
                lblPhoneNumber.setVisibility(View.VISIBLE);
                tvPhoneNumber.setText(" " + result.getFormattedPhoneNumber());
            } else {
                removeView(lblPhoneNumber);
                removeView(tvPhoneNumber);
                removeView(bulletPhone);
            }

            if (result.getInternationalPhoneNumber() != null) {

                lblInternationalPhoneNumber.setVisibility(View.VISIBLE);
                tvInternationalPhoneNumber.setText(" " + result.getInternationalPhoneNumber());
            } else {
                removeView(lblInternationalPhoneNumber);
                removeView(tvInternationalPhoneNumber);
                removeView(bulletPhone2);
            }


            if (result.getWebsite() != null) {
                lblWebsite.setVisibility(View.VISIBLE);
                tvWebsite.setText(" " + result.getWebsite());
            } else {
                removeView(lblWebsite);
                removeView(tvWebsite);
                removeView(bulletWebsite);
            }

            latLongOfAPlace = new LatLng(Double.parseDouble(aSinglePlace.get("lat")),
                    Double.parseDouble(aSinglePlace.get("lng")));
            latLongOfDevice = new LatLng(latLongOfUser[0], latLongOfUser[1]);

            if (result.getPhotos() != null) {
                final List<Photo> listOfPhotos = result.getPhotos();

                final ImageView[] imageViews = new ImageView[listOfPhotos.size()];
                int width = (int) getResources().getDimension(R.dimen.for_popular_places_photos);
                int length = (int) getResources().getDimension(R.dimen.for_popular_places_photos);

                for (int i = 0; i < listOfPhotos.size(); i++) {
                    imageViews[i] = new ImageView(PlaceDetailsActivity.this);

                    Glide.with(this).load(getImageFromReference(listOfPhotos.get(i).getPhotoReference())).into(imageViews[i]);

                    imageViews[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageViews[i].setLayoutParams(new LinearLayoutCompat.LayoutParams(Resources.getSystem().getDisplayMetrics().widthPixels,Resources.getSystem().getDisplayMetrics().widthPixels));
                    layForPic2.addView(imageViews[i]);
                }
            } else {
                removeView(layForPic2);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
    }

    public void removeView(View view) {
        ViewGroup vg = (ViewGroup) (view.getParent());
        vg.removeView(view);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;

        deviceLocation = googleMap.addMarker(new MarkerOptions()
                .position(latLongOfDevice)
                .title("User's Current Location")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
        destinationLocation = googleMap.addMarker(new MarkerOptions().position(latLongOfAPlace).title(aSinglePlace.get("place_name")));


        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLongOfDevice, 15));

        GetDirectionsData getDirectionsData = new GetDirectionsData(this);
        getDirectionsData.execute(latLongOfDevice, latLongOfAPlace, googleMap);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
    }

    private String getImageFromReference(String reference){

        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/photo?");
        sb.append("maxwidth=1600");
        sb.append("&photoreference=" + reference);
        sb.append("&key=" + "AIzaSyCYveG9Qow1kfrG4d12aq6hHhFTbqGb4-I");

        return sb.toString();
    }


    public void syncLocationTwo(View view){
        deviceLocation.remove();
        polyline.remove();
        getCoordinates();
    }

    public void fetchPolyLine(Polyline polyline){
        this.polyline = polyline;
    }

    private void getCoordinates() {
        FusedLocationProviderClient mFusedLocationClient = mFusedLocationClient = getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
            @Override
            public void onSuccess(android.location.Location location) {
                if (location != null) {


                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    //Toast.makeText(PlaceDetailsActivity.this, "Location fetch success.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(PlaceDetailsActivity.this, "Lat: " + latitude + " Long: + " + longitude, Toast.LENGTH_SHORT).show();

                    latLongOfUser[0] = latitude;
                    latLongOfUser[1] = longitude;

                    LatLng deviceLatLng = new LatLng(latLongOfUser[0],latLongOfUser[1]);


                    deviceLocation = mGoogleMap.addMarker(new MarkerOptions().position(deviceLatLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(deviceLatLng, 15));


                    GetDirectionsData getDirectionsData = new GetDirectionsData(PlaceDetailsActivity.this);
                    try {
                        getDirectionsData.execute(deviceLatLng, latLongOfAPlace, mGoogleMap).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                } else {
                    //Toast.makeText(PlaceDetailsActivity.this, "Location fetch failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
