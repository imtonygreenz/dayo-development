package com.jalm.dayo;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by tranc on 27/09/2017.
 */

public class LocationInitializer {

    private LocationInitializerListener listener;

    public interface LocationInitializerListener{
        public void onMarkerLoaded(Marker currentLocation);
    }
}

