package com.jalm.dayo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.jalm.dayo.MapActivity;
import com.jalm.dayo.databaseatbp.DayoDatabaseHelper;

import java.util.Random;

/**
 * Created by tranc on 25/09/2017.
 */

public class GetAudioMiscData extends AsyncTask<String, Void, Cursor>{
    Activity activity;
    SQLiteOpenHelper popularDatabaseHelper;
    SQLiteDatabase database;


    GetAudioMiscData(Activity activity){
        this.activity = activity;
    }

    @Override
    protected Cursor doInBackground(String... strings) {

        popularDatabaseHelper = new DayoDatabaseHelper(activity);
        database = popularDatabaseHelper.getWritableDatabase();

        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(activity);
        String selectedTourVoice = SP.getString("selectedTourVoice","1");

        String table = ""; //could TB_TA or TB_EA of DayoDatabaseHelper

        if(selectedTourVoice.equals("1")){ //GIA
            table = DayoDatabaseHelper.TB_TMA;
        }else if(selectedTourVoice.equals("2")){
            table = DayoDatabaseHelper.TB_EMA;
        }



        Cursor cursor = database.query(table, new String[]{"RESOURCEID"}, null, null,
                null,null,null);

        return cursor;
    }

    @Override
    protected void onPostExecute(final Cursor cursor) {
        super.onPostExecute(cursor);

        AlertDialog.Builder dialog = new AlertDialog.Builder((MapActivity) activity);

        ((MapActivity) activity).playAudio(R.raw.aud_alert);

        dialog.setTitle("Prompt");
        dialog.setMessage("You have exited a geofence. Would you like to listen for some information?");
        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int numberOfAudioMisc = cursor.getCount();

                Random random = new Random();
                int randomNumber = random.nextInt(numberOfAudioMisc) + 0;

                cursor.moveToPosition(randomNumber);

                int media = cursor.getInt(0);

                ((MapActivity) activity).playAudio(media);
                dialog.dismiss();

            }
        });
        dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();


    }
}
