package com.jalm.dayo;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

import com.jalm.dayo.databaseatbp.DayoDatabaseHelper;

/**
 * Created by tranc on 19/09/2017.
 */

public class GetGeofencesData extends AsyncTask<Cursor, Void, Cursor> {

    Activity activity;

    SQLiteOpenHelper popularDatabaseHelper;
    SQLiteDatabase database;

    public GetGeofencesData(Activity activity){
        this.activity = activity;
    }


    @Override
    protected Cursor doInBackground(Cursor... cursors) {
        popularDatabaseHelper = new DayoDatabaseHelper(activity);
        database = popularDatabaseHelper.getWritableDatabase();

        Cursor cursor = database.query(DayoDatabaseHelper.TB_PP, new String[]{"LATITUDE", "LONGITUDE", "_id"},
                null,null,null,null,null);



        return cursor;

    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        super.onPostExecute(cursor);

        popularDatabaseHelper.close();
        database.close();
    }
}
