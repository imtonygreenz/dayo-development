package com.jalm.dayo;

import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by tranc on 19/08/2017.
 * Class retrieves URL meaning it fetches data on JSON format
 */

public class DownloadURL {

    public String readURL(String myURL) throws IOException, NullPointerException {

        String data = ""; //variable for the whole JSON text turned into a String
        InputStream inputStream = null; //reads the raw data and converts it into something that is readable
        HttpURLConnection urlConnection = null; //used for Http features

        try {
            URL url = new URL(myURL); //a pointer to the resource, meaning, a pointer to the myURL argument
            urlConnection = (HttpURLConnection) url.openConnection();//creates a connection with the pointer
            urlConnection.connect();//starts the retrieval, another note, this method should be called within an AsyncTask, bawal sa UI thread

            inputStream = urlConnection.getInputStream(); //converts the data that has been gathered into a readable format
            BufferedReader x = new BufferedReader(new InputStreamReader(inputStream));//the readable format is further converted to a much more better format, likewise, it transforms bits into readable words or ASCIIs
            StringBuffer appender = new StringBuffer();//appends or joins the converted text from the buffered reader

            String line = "";//reads the buffer or storage, line by line

            while((line = x.readLine())!= null){
                appender.append(line);
            }

            data = appender.toString();
            x.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        finally{
            inputStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}
