package com.jalm.dayo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.*;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jalm.dayo.databaseatbp.DayoDatabaseHelper;

import java.util.concurrent.ExecutionException;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

//TODO: FIx animation bug

public class PopularPlacesDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String intentASINGLEPOPULARPLACE = "aSinglePopularPlace";
    public static final String intentLATLONGOFUSER = "coordinates";

    private GoogleMap mGoogleMap;
    //private ArrayList<Integer> listOfResourceImage = new ArrayList<Integer>();

    TextView tvPPlaceName;
    TextView tvPAddress;
    TextView tvPDescription;
    LinearLayout layForPic;

    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;

    private int[] listOfImageViewId;

    private double[] latLongOfUser = new double[2];

    Polyline polyline;

    private double destinationLat;
    private double destinationLng;

    private Marker deviceLocationMarker; //this will change depending on the location of the device
    private Marker destinationLocationMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        long _id = intent.getLongExtra(intentASINGLEPOPULARPLACE, 0);
        latLongOfUser = intent.getDoubleArrayExtra(intentLATLONGOFUSER);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_places_details);

        final ScrollView svPopularPlaces = (ScrollView) findViewById(R.id.sv_popularPlaces);
        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        svPopularPlaces.requestDisallowInterceptTouchEvent(true);
                        Log.d("ONTOUCH: ", "Down");
                        return false;
                    case MotionEvent.ACTION_UP:
                        svPopularPlaces.requestDisallowInterceptTouchEvent(false);
                        Log.d("ONTOUCH: ", "Up");
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        svPopularPlaces.requestDisallowInterceptTouchEvent(true);
                        Log.d("ONTOUCH: ", "Move");
                        return false;
                    default:
                        return true;
                }
            }
        });

        layForPic = (LinearLayout) findViewById(R.id.layForPic);

        tvPPlaceName = (TextView) findViewById(R.id.tvPPlaceName);
        tvPAddress = (TextView) findViewById(R.id.tvPAddress);
        tvPDescription = (TextView) findViewById(R.id.tvPDescription);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().
                findFragmentById(R.id.pmap);
        mapFragment.getMapAsync(this);

        PopularPlacesDetailsActivity.GetPopularPlaceData getPopularPlaceData =
                new PopularPlacesDetailsActivity.GetPopularPlaceData();

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        try {
            getPopularPlaceData.execute(_id).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
    }

    private class GetPopularPlaceData extends AsyncTask<Object, Void, Cursor>{

        @Override
        protected Cursor doInBackground(Object... objects) {
            long id = (long) objects[0];

            String sql = "SELECT PP.NAME, PP.ADDRESS, PP.DESCRIPTION, " +
                    "PP.LATITUDE, PP.LONGITUDE, " +
                    "P.RESOURCEID FROM PICTUREJOIN as PJ " +
                    "JOIN PICTURE as P ON PJ.PICID = P.PICID " +
                    "JOIN POPULARPLACES as PP ON PJ.PPID = PP._id " +
                    "WHERE PJ.PPID = " + id + ";";

            SQLiteOpenHelper databaseHelper = new DayoDatabaseHelper(PopularPlacesDetailsActivity.this);
            SQLiteDatabase database = databaseHelper.getReadableDatabase();


            Cursor cursor = database.rawQuery(sql, null);

            return cursor;
        }

        @Override
        protected void onPostExecute(final Cursor cursor) {

            Handler handler = new Handler(getMainLooper());
            handler.post(new Runnable(){

                @Override
                public void run() {
                    cursor.moveToFirst();

                    destinationLat = cursor.getDouble(3);
                    destinationLng = cursor.getDouble(4);

                    tvPPlaceName.setText(cursor.getString(0));
                    tvPAddress.setText(cursor.getString(1));
                    tvPDescription.setText(cursor.getString(2));

                    LatLng device = new LatLng(latLongOfUser[0],latLongOfUser[1]);
                    LatLng destination = new LatLng(destinationLat,destinationLng);


                    addMarkers(device, destination, mGoogleMap);

                    final ImageView[] imageViews = new ImageView[cursor.getCount()];
                    listOfImageViewId = new int[cursor.getCount()];

                    //int width = (int)layForPic.getLayoutParams().
                    //int length = (int)getResources().getDimension(R.dimen.for_popular_places_photos);

                    for(int i = 0; i < cursor.getCount(); i++){
                        //listOfResourceImage.add(cursor.getInt(5));
                        imageViews[i] = new ImageView(PopularPlacesDetailsActivity.this);
                        //imageViews[i].setImageDrawable(scaleImage(getResources().getDrawable(cursor.getInt(5)), 0.01f));

                        Glide.with(PopularPlacesDetailsActivity.this).load(cursor.getInt(5)).into(imageViews[i]);

                        imageViews[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                        imageViews[i].setLayoutParams(new LinearLayoutCompat.LayoutParams(Resources.getSystem().getDisplayMetrics().widthPixels,Resources.getSystem().getDisplayMetrics().widthPixels));

                        cursor.moveToNext();

                        layForPic.addView(imageViews[i]);
                    }
                }
            });
        }


        public Drawable scaleImage(Drawable image, float scaleFactor){
            Bitmap b = ((BitmapDrawable)image).getBitmap();

            int sizeX = Math.round(image.getIntrinsicWidth() * scaleFactor);
            int sizeY = Math.round(image.getIntrinsicHeight() * scaleFactor);

            Bitmap bitmapResized = Bitmap.createScaledBitmap(b, sizeX, sizeY, false);
            image = new BitmapDrawable(getResources(), bitmapResized);

            return image;
        }

        public void addMarkers(LatLng latLngOfDevice,LatLng latLngOfDestination, GoogleMap map){


            destinationLocationMarker = mGoogleMap.addMarker(new MarkerOptions().position(latLngOfDestination));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngOfDevice, 15));

            deviceLocationMarker = mGoogleMap.addMarker(new MarkerOptions().position(latLngOfDevice)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))); //this will change

            GetDirectionsData getDirectionsData = new GetDirectionsData(PopularPlacesDetailsActivity.this);
            try {
                getDirectionsData.execute(latLngOfDevice, latLngOfDestination , mGoogleMap).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public void syncLocation(View view){
        eraseMapLegends();
    }

    public void fetchPolyLine(Polyline polyline){
        this.polyline = polyline;
    }

    private void eraseMapLegends(){
        deviceLocationMarker.remove();
        polyline.remove();
        getCoordinates();
    }

    private void getCoordinates() {
        FusedLocationProviderClient mFusedLocationClient = mFusedLocationClient = getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {


                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    //Toast.makeText(PopularPlacesDetailsActivity.this, "Location fetch success.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(PopularPlacesDetailsActivity.this, "Lat: " + latitude + " Long: + " + longitude, Toast.LENGTH_SHORT).show();

                    latLongOfUser[0] = latitude;
                    latLongOfUser[1] = longitude;

                    LatLng deviceLocation = new LatLng(latLongOfUser[0],latLongOfUser[1]);
                    LatLng destinationLocation = new LatLng(destinationLat,destinationLng);

                    deviceLocationMarker = mGoogleMap.addMarker(new MarkerOptions().position(deviceLocation)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(deviceLocation, 15));


                    GetDirectionsData getDirectionsData = new GetDirectionsData(PopularPlacesDetailsActivity.this);
                    try {
                        getDirectionsData.execute(deviceLocation, destinationLocation, mGoogleMap).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                } else {
                    //Toast.makeText(PopularPlacesDetailsActivity.this, "Location fetch failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
