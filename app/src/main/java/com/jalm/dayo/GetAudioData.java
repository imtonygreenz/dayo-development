package com.jalm.dayo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.jalm.dayo.databaseatbp.DayoDatabaseHelper;

/**
 * Created by tranc on 19/09/2017.
 */

public class GetAudioData extends AsyncTask<String, Void, Cursor>{

    Activity activity;

    Handler handler;
    AlertDialog alertDialog;

    SQLiteOpenHelper popularDatabaseHelper;
    SQLiteDatabase database;

    GetAudioData(Activity activity){
        this.activity = activity;
    }


    @Override
    protected Cursor doInBackground(String... strings) {

        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(activity);
        String selectedTourVoice = SP.getString("selectedTourVoice","1");

        String table = ""; //could TB_TA or TB_EA of DayoDatabaseHelper

        if(selectedTourVoice.equals("1")){ //GIA
            table = DayoDatabaseHelper.TB_TA;
        }else if(selectedTourVoice.equals("2")){
            table = DayoDatabaseHelper.TB_EA;
        }

        popularDatabaseHelper = new DayoDatabaseHelper(activity);
        database = popularDatabaseHelper.getWritableDatabase();



        Cursor cursor = database.query(table, new String[]{"RESOURCEID"}, "AID = ?", new String[]{strings[0]},
                null,null,null);



        return cursor;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {

        super.onPostExecute(cursor);

        handler = new Handler();

        cursor.moveToFirst();

        if(cursor.getInt(0) == 0){
            AlertDialog.Builder dialog = new AlertDialog.Builder((MapActivity) activity, R.style.AlertDialogTheme);
            dialog.setTitle("Prompt");
            dialog.setMessage("You have entered a geofence. Unfortunately, there is no available audio data " +
                    "for this place at the moment. Try changing your tour assistant in order to play the audio data of this place.");

            dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    handler.removeCallbacks(new RunMe());
                    dialogInterface.dismiss();
                }
            });

            alertDialog = dialog.create();
            alertDialog.show();

            popularDatabaseHelper.close();
            database.close();
        }else{
            AlertDialog.Builder dialog = new AlertDialog.Builder((MapActivity) activity, R.style.AlertDialogTheme);
            dialog.setTitle("Prompt");
            dialog.setMessage("You have entered a geofence.");

            dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                    handler.removeCallbacks(new RunMe());
                    dialogInterface.dismiss();
                }
            });

            alertDialog = dialog.create();
            alertDialog.show();

            handler.postDelayed(new RunMe(), 3000);

            cursor.moveToFirst();
            int media = cursor.getInt(0);

            ((MapActivity) activity).playTourSample(media);

            popularDatabaseHelper.close();
            database.close();
        }



    }

    private class RunMe implements Runnable{
        @Override
        public void run() {
            if(alertDialog.isShowing()){
                alertDialog.dismiss();
            }
        }
    }
}
