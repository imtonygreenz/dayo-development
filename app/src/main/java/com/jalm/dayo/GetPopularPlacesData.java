package com.jalm.dayo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.jalm.dayo.databaseatbp.DayoDatabaseHelper;

/**
 * Created by tranc on 10/09/2017.
 */


public class GetPopularPlacesData extends AsyncTask<Object, Void, Cursor> {

    HomeActivity activity;

    SQLiteOpenHelper popularDatabaseHelper;
    SQLiteDatabase database;

    public GetPopularPlacesData(HomeActivity referencedActivity){
        activity = referencedActivity;
    }

    @Override
    protected Cursor doInBackground(Object... objects) {
        popularDatabaseHelper = new DayoDatabaseHelper(activity);
        database = popularDatabaseHelper.getWritableDatabase();



        Log.d("Database: ", "Database version is " + database.getVersion());

//        Cursor cursor = database.query("POPULARPLACES",
//                                        new String[]{"_id","NAME", "ADDRESS", "DESCRIPTION",
//                                                "LATITUDE", "LONGITUDE"}, null, null, null, null, null);


        //sql returns one picture per place
        String sql = "SELECT PP._id, P.RESOURCEID, PP.NAME FROM POPULARPLACES AS PP " +
                "JOIN PICTUREJOIN AS PJ ON PP._id = PJ.PPID " +
                "JOIN PICTURE AS P ON PJ.PICID = P.PICID " +
                "GROUP BY PP._id " +
                "HAVING MIN(PP._id)" +
                "ORDER BY PP._id";

        Cursor cursor = database.rawQuery(sql,null);



        return cursor;
    }

    @Override
    protected void onPostExecute(final Cursor cursor) {
        super.onPostExecute(cursor);

        activity.settingTheAdapter(cursor);
        popularDatabaseHelper.close();
        database.close();
    }
}
