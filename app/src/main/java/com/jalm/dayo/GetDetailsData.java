package com.jalm.dayo;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;


import com.google.gson.Gson;

import com.jalm.dayo.forgsongetdetails.*;

import java.io.IOException;

/**
 * Created by tranc on 08/09/2017.
 */

public class GetDetailsData extends AsyncTask<Object, Void, Details> {

    private PlaceDetailsActivity activity;
    private Details details;

    public GetDetailsData(PlaceDetailsActivity activityToBeReferenced){
        this.activity = activityToBeReferenced;
    }

    @Override
    protected Details doInBackground(Object... objects) {

        try {
            DownloadURL downloadURL = new DownloadURL();
            String rawDataInJSONFormat = downloadURL.readURL(getUrl((String) objects[0]));

            Gson gson = new Gson();
            details = gson.fromJson(rawDataInJSONFormat, Details.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return details;
    }

    @Override
    protected void onPostExecute(Details details) {


//        Handler handler = new Handler(Looper.getMainLooper());
//        handler.post(new Runnable(){
//
//            @Override
//            public void run() {
//
//                TextView tvAddress = (TextView)activity.findViewById(R.id.tvAddress);
//                TextView tvPhoneNumber = (TextView)activity.findViewById(R.id.tvPhoneNumber);
//                TextView tvInternationalPhoneNumber = (TextView)activity.findViewById(R.id.tvInternationalPhoneNumber);
//                TextView tvWeekdayText = (TextView)activity.findViewById(R.id.tvWeekdayText);
//                TextView tvWebsite = (TextView)activity.findViewById(R.id.tvWebsite);
//
//                Result result = details.getResult();
//
//                tvAddress.setText(result.getFormattedAddress());
//                tvPhoneNumber.setText(result.getFormattedPhoneNumber());
//                tvInternationalPhoneNumber.setText(result.getInternationalPhoneNumber());
//
//                StringBuilder weekdayText = new StringBuilder();
//
//                //for(int i = 0; i < result.getOpeningHours().getWeekdayText().size(); i++){
//                //    weekdayText.append(result.getOpeningHours().getWeekdayText().get(i));
//                //}
//
//
//                tvWeekdayText.setText(weekdayText.toString());
//                tvWebsite.setText(result.getWebsite());
//            }
//        });

        super.onPostExecute(details);
    }

    private String getUrl(String reference) {

        StringBuilder googleDirectionUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
        googleDirectionUrl.append("reference=" + reference);
        googleDirectionUrl.append("&key=" + "AIzaSyCYveG9Qow1kfrG4d12aq6hHhFTbqGb4-I");

        return googleDirectionUrl.toString();
    }
}
