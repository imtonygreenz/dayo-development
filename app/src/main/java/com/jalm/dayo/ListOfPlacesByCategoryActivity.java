package com.jalm.dayo;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jalm.dayo.forgsongetsearchresult.Result;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class ListOfPlacesByCategoryActivity extends AppCompatActivity {

    public static final String intentLISTOFPLACES = "listOfPlacesForIntent";
    public static final String intentLATLONG = "latLongForIntent";

    private List<HashMap<String,String>> listOfPlaces;
    private int selectedPlace = 0; //kukunin nito kung pang-ilan yung naselect na item sa list view, gagamitin sa pagreretrieve ng single place sa hash map
    private double[] latLongOfUser;

    private List<Result> listOfResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_places_by_category);


        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            Bundle appData = intent.getBundleExtra(SearchManager.APP_DATA);
            latLongOfUser = appData.getDoubleArray(intentLATLONG);

            GetNearbyPlacesDataForSearchResult getNearbyPlacesDataForSearchResult = new GetNearbyPlacesDataForSearchResult(this);
            try {
                listOfPlaces = getNearbyPlacesDataForSearchResult.execute(getUrl(query)).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }else{
            listOfPlaces = (List<HashMap<String,String>>) intent.getSerializableExtra(intentLISTOFPLACES);
            latLongOfUser = intent.getDoubleArrayExtra(PlaceDetailsActivity.intentUSERLATLONG);
        }


            String [] nameOfPlaces = new String[listOfPlaces.size()];

            for(int i = 0; i < listOfPlaces.size(); i++){
                nameOfPlaces[i] = listOfPlaces.get(i).get("place_name");
            }

            ListView lvPlaces = (ListView) findViewById(R.id.lvPlaces);
            ArrayAdapter<String> lvPlacesAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, nameOfPlaces);

            lvPlaces.setAdapter(lvPlacesAdapter);


            lvPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    TextView aSingleItem = (TextView)view;
                    String nameOfPlace = "" + aSingleItem.getText();

                    //Toast.makeText(getApplicationContext(), nameOfPlace + " is selected.", Toast.LENGTH_SHORT).show();
                    selectedPlace = i; //will be used sa hashMap

                    HashMap<String,String> aSinglePlace = listOfPlaces.get(selectedPlace);


                    //might move this to GetDetailsData.java, the intent calling should be in the ASyncTask
                    //para maretrieve na agad yung Place details sa pamamagitan ng reference, then istostore yung
                    //naretrieve na Place details sa isang list then show it
                    //this avoids the background task kasi, naretrieve na agad yung data bago pa maidisplay yung activity
                    //instead of showing the activity and some data, then retrieve then show which is a hassle
                    //retrieve muna bago show...

                    Intent intent = new Intent(getApplicationContext(), PlaceDetailsActivity.class);
                    intent.putExtra(PlaceDetailsActivity.intentASINGLEPLACE, (Serializable) aSinglePlace);
                    intent.putExtra(PlaceDetailsActivity.intentUSERLATLONG, latLongOfUser);
                    startActivity(intent);
                }
            });
    }

    private String getUrl(String keyword) {

        Scanner scanner = new Scanner(keyword);
        String place = scanner.nextLine();
        place = place.replace(" ", "+");

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/textsearch/json?query=");
        googlePlaceUrl.append(place);
        googlePlaceUrl.append("+in+Lucena+City+Quezon&key=AIzaSyCYveG9Qow1kfrG4d12aq6hHhFTbqGb4-I");
        return googlePlaceUrl.toString();
    }



    @Override
    protected void onPause() {
        super.onPause();
    }
}
