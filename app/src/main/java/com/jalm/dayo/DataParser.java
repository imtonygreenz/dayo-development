package com.jalm.dayo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tranc on 19/08/2017.
 */

public class DataParser {

    //gets a single place from a bunch of places based on the JSONData
    private HashMap<String, String>getPlace(JSONObject googlePlaceJson) {
        HashMap<String, String> googlePlaceMap = new HashMap<>();

        //variable initialization
        String placeName = "-NA-";
        String vicinity = "-NA-";
        String latitude = "";
        String longitude = "";
        String reference = "";

        try {
            if (!googlePlaceJson.isNull("name")) {
                placeName = googlePlaceJson.getString("name");
            }

            if (!googlePlaceJson.isNull("vicinity")) {
                vicinity = googlePlaceJson.getString("vicinity");
            }

            latitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location")
                    .getString("lat");
            longitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location")
                    .getString("lng");

            reference = googlePlaceJson.getString("reference");

            googlePlaceMap.put("place_name", placeName);
            googlePlaceMap.put("vicinity", vicinity);
            googlePlaceMap.put("lat", latitude);
            googlePlaceMap.put("lng", longitude);
            googlePlaceMap.put("reference", reference);

        } catch(JSONException e){
            e.printStackTrace();
        }

        return googlePlaceMap; //returns a single place that contains its name, vicinity, geometry, and reference
    }

    //a method that fetches and converts the JSONData which are places into singular items that composes a list
    private List<HashMap<String, String>> getPlaces(JSONArray jsonArray){
        int count = jsonArray.length();
        List<HashMap<String, String>> placesList = new ArrayList<>();
        HashMap<String,String> placeMap = null;

        for(int i = 0; i < count; i++){//the part of the code where the separation of places into singular items happen
                try {
                placeMap = getPlace((JSONObject) jsonArray.get(i));//this calls the method that himay himay the places into single items
                placesList.add(placeMap);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return placesList;
    }

    //this method takes the result of the google web search api from the GetNearbyPlacesData and then
    //calls the getPlaces method above
    public List<HashMap<String,String>> parse(String jsonData) throws NullPointerException{
        JSONArray jsonArray = null;
        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject((jsonData));
            jsonArray = jsonObject.getJSONArray("results");//fetches the "results" object of the JSONData and stores it here
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getPlaces(jsonArray);
    }
}
