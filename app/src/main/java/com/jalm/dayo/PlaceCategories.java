package com.jalm.dayo;

/**
 * Created by tranc on 19/08/2017.
 */

public class PlaceCategories {

    private int picture;
    private String categoryName;

    public static final PlaceCategories[] placeCategories = new PlaceCategories[]{
            //TODO: might as well, improve these categories and convert the list into a grid type
            new PlaceCategories(R.drawable.icon_cat_hospital,"Hospitals"),
            new PlaceCategories(R.drawable.icon_cat_government,"Government"),
            new PlaceCategories(R.drawable.icon_cat_school,"Schools"),
            new PlaceCategories(R.drawable.icon_cat_restaurant,"Foods"),
            new PlaceCategories(R.drawable.icon_cat_bank,"Banks"),
            new PlaceCategories(R.drawable.icon_cat_salon,"Salons"),
            new PlaceCategories(R.drawable.icon_cat_church  , "Churches"),
            new PlaceCategories(R.drawable.icon_cat_conveniencestore,"Stores"),
            new PlaceCategories(R.drawable.icon_cat_park, "Parks"),
            new PlaceCategories(R.drawable.icon_cat_drugstore, "Pharmacies")
    };

    private PlaceCategories(int picture, String categoryName){
        this.picture = picture;
        this.categoryName = categoryName;
    }

    private PlaceCategories(String categoryName){
        this.picture = 0;
        this.categoryName = categoryName;
    }

    public String getPlaceCategoriesName(){
        return this.categoryName;
    }

    public int getPlaceCategoriesPicture(){
        return this.picture;
    }
}
