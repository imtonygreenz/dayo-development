package com.jalm.dayo;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.*;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.jalm.dayo.forgsongetdirections.*;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tranc on 08/09/2017.
 */

public class GetDirectionsData extends AsyncTask<Object, Void, Void> {

    private Activity activity;
    private Directions directions;
    private List<LatLng> routeCoordinates;
    private GoogleMap myGoogleMap;
    private OverviewPolyline overviewPolyline;

    public GetDirectionsData(Activity callerActivity){
        this.activity = callerActivity;
    }

    //fetches the LatLong of device and LatLong of destination
    @Override
    protected Void doInBackground(Object... objects) {

        try {
            LatLng deviceLocation = (LatLng) objects[0];
            LatLng destinationLocation = (LatLng) objects[1];
            myGoogleMap = (GoogleMap) objects[2];

            DownloadURL downloadURL = new DownloadURL();
            String rawDataInJSONFormat = downloadURL.readURL(getUrl(deviceLocation,destinationLocation));

            Gson gson = new GsonBuilder().create();
            directions = gson.fromJson(rawDataInJSONFormat, Directions.class);

            overviewPolyline = directions.getRoutes().get(0).getOverviewPolyline();
            routeCoordinates = decodePoly(overviewPolyline.getPoints());

        } catch (IOException e) {
            e.printStackTrace();
        }



        return null;
    }

    //builds the url
    private String getUrl(LatLng origin, LatLng destination) {

        StringBuilder googleDirectionUrl = new StringBuilder("https://maps.googleapis.com/maps/api/directions/json?");
        googleDirectionUrl.append("origin=" + origin.latitude + "," + origin.longitude);
        googleDirectionUrl.append("&destination="  + destination.latitude + "," + destination.longitude);
        googleDirectionUrl.append("&mode=" + "walking");
        googleDirectionUrl.append("&transit_routing_preference=" + "less_walking");
        googleDirectionUrl.append("&key=" + "AIzaSyCYveG9Qow1kfrG4d12aq6hHhFTbqGb4-I");

        return googleDirectionUrl.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        Handler handler = new Handler(activity.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {

                PolylineOptions directionsPolylineOptions = new PolylineOptions();

                for(int i = 0; i <  routeCoordinates.size(); i++){

                    LatLng aSingleCoordinate = routeCoordinates.get(i);

                    directionsPolylineOptions.add(aSingleCoordinate);
                }

                Polyline polyline =  myGoogleMap.addPolyline(directionsPolylineOptions
                        .width(10)
                        .color(Color.RED));

                if(activity instanceof PopularPlacesDetailsActivity){
                    PopularPlacesDetailsActivity ppActivity = (PopularPlacesDetailsActivity)activity;
                    ppActivity.fetchPolyLine(polyline);
                }else if(activity instanceof PlaceDetailsActivity){
                    ((PlaceDetailsActivity) activity).fetchPolyLine(polyline);
                }




                //Toast.makeText(activity, "Directions successfully retrieved!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}
