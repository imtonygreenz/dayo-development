package com.jalm.dayo;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;


public class GeofenceTransitionsIntentService extends IntentService {

    private static final String TAG = "GeofenceTransitionsIS";



    public GeofenceTransitionsIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        Log.d("Geofence", "I triggered a geofence.");


        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
            geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL) {
            Log.d("Geofence", "I entered a geofence.");
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            String[] triggerIds = new String[triggeringGeofences.size()];

            for (int i = 0; i < triggerIds.length; i++) {
                triggerIds[i] = triggeringGeofences.get(i).getRequestId();
            }

            if(MyApplication.isActivityVisible()){
                Intent intent2 = new Intent("some_custom_id");
                intent2.putExtra(MapActivity.intentTRIGGEREDGEOFENCES, triggerIds);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
            }else{
                Intent intent2 = new Intent("some_custom_id");
                intent2.putExtra(MapActivity.intentTRIGGEREDGEOFENCES, triggerIds);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
            }
        }else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT){

            Log.d("Geofence", "I exited a geofence.");

            if(MyApplication.isActivityVisible()){
                Intent intent2 = new Intent("for_audio_walk");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
            }else{
                Log.d("Geofence", "Broadcast not sent.");
                Intent intent2 = new Intent("for_audio_walk");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
            }
        }
    }


}
