package com.jalm.dayo;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.app.DialogFragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_EXIT;
import static com.google.android.gms.location.Geofence.NEVER_EXPIRE;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static com.jalm.dayo.R.id.map;

public class MapActivity extends AppCompatActivity implements
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    //national highway 13.935255, 121.601612
    //bahay ko 13.935039, 121.601300
    //cris coords 13.972682, 121.553715

    //TODO: Show details about the place on the dialog box

    public static final String intentTRIGGEREDGEOFENCES = "geofences";
    public static final String intentINITLATLONG = "coordinates";

    private static double MYLATITUDE = 13.935039;
    private static double MYLONGITUDE = 121.601300;
    private static int RADIUS = 50;

    private double[] latLongOfUser = new double[2];
    int forInitialTrigger = 0;

    GoogleMap mgoogleMap;
    GoogleApiClient mGoogleApiClient;
    Marker mCurrLocationMarker = null;
    LocationRequest mLocationRequest;
    boolean tShowed = false;

    private double[] initCoords;

    private String[] triggeredGeofencesId = new String[0];

    private MediaPlayer player = new MediaPlayer();
    MediaPlayer player2 = new MediaPlayer();

    private GeofencingClient mGeofencingClient;
    private PendingIntent mGeofencePendingIntent;
    private ArrayList<Geofence> mGeofenceList = new ArrayList<Geofence>();
    Calendar calendar = Calendar.getInstance();
    int mtime = calendar.get(Calendar.HOUR_OF_DAY);


    Cursor geofencesData;

    Markers[] circlesAndMarker;


    protected BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            triggeredGeofencesId = intent.getStringArrayExtra(intentTRIGGEREDGEOFENCES);

            GetAudioData getAudioData = new GetAudioData(MapActivity.this);
            getAudioData.execute(triggeredGeofencesId);
        }
    };

    protected BroadcastReceiver mNotificationReceiverForAudioWalk = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            GetAudioMiscData getAudioMiscData = new GetAudioMiscData(MapActivity.this);
            getAudioMiscData.execute();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //checking of google services

        //Intent intent = getIntent();
        //initCoords = intent.getDoubleArrayExtra(intentINITLATLONG);

        getCoordinates();



        if (googleServicesAvailable()) {
            //Toast.makeText(this, "Connected to Google Play Services", Toast.LENGTH_LONG).show();
            setContentView(R.layout.activity_map);
            showWelcomeDialog();

        } else {
            // No Map Layout
            //Toast.makeText(this, "Cannot connect to Google Play Services", Toast.LENGTH_LONG).show();
        }


        mGeofencingClient = LocationServices.getGeofencingClient(this);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }




        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter("some_custom_id"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiverForAudioWalk, new IntentFilter("for_audio_walk"));


    }

    private void initMap() {
        MapFragment map = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment));
        map.getMapAsync(this); //initiates the map on the map fragment
    }

    private void showWelcomeDialog() {
//        DialogFragment welcomeDialog = new NavigationModeWelcomeDialog();
//        welcomeDialog.show(getFragmentManager(),"WelcomeDialogFragment");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String name = " " + sp.getString("username","") + " ";

        if(name.equals("")){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
            dialog.setTitle("Welcome");
            dialog.setMessage("Mabuhay and Welcome to Dayo! Your tour assistant will help you in" +
                    " navigating your ways to explore the City of Lucena.");
            dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    initMap();
                }
            });
            dialog.show();

        }else{
            AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
            dialog.setTitle("Welcome");
            dialog.setMessage("Mabuhay" + name + "and Welcome to Dayo! Your tour assistant will help you in" +
                    " navigating your ways to explore the City of Lucena.");
            dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    initMap();
                }
            });
            dialog.show();

        }



    }

    public boolean googleServicesAvailable() {
        // method checking if there is google services on the device or can connect to it

        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            //Toast.makeText(this, "Cannot connect to Google Play Services.", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mgoogleMap = googleMap;

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build(); //builds the map and requests for location
        mGoogleApiClient.connect(); //tries to connect to google api client for locationrequests
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mgoogleMap.setMyLocationEnabled(true);

        //for debugging purposes


        //goToLocationZoom(initCoords[0], initCoords[1], 10);





        try {
            createGeofences();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        circlesAndMarker = new Markers[geofencesData.getCount()];

        geofencesData.moveToFirst();

        for(int i = 0; i < geofencesData.getCount(); i++){
            circlesAndMarker[i] = markerForGeofence(new LatLng(geofencesData.getDouble(0),geofencesData.getDouble(1)));
            geofencesData.moveToNext();
        }


        for(int i = 0; i < circlesAndMarker.length; i++){

            CircleOptions circle = circlesAndMarker[i].getCircleOptions();
            float[] distance = new float[2];

            Location.distanceBetween( latLongOfUser[0],latLongOfUser[1],
                    circle.getCenter().latitude, circle.getCenter().longitude, distance);

            if( distance[0] > circle.getRadius()  ){
                //Toast.makeText(getBaseContext(), "Outside", Toast.LENGTH_LONG).show();
                forInitialTrigger = 0;
            } else {
                //Toast.makeText(getBaseContext(), "Inside", Toast.LENGTH_LONG).show();
                forInitialTrigger = GeofencingRequest.INITIAL_TRIGGER_ENTER;
                break;
            }
        }

        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Toast.makeText(MapActivity.this, "Geofences have been added.", Toast.LENGTH_SHORT).show();
                    }
                });



    }

    private void goToLocationZoom(double lat, double lng, int zoom) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mgoogleMap.moveCamera(update);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mapTypeNone:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
            case R.id.mapTypeNormal:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.mapTypeSatellite:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.mapTypeTerrain:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.mapTypeHybrid:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) { // if google api client connects
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); // high accuracy but consumes more battery life
        mLocationRequest.setInterval(1000); //time for location requests set to 1s

        //this is for android n only checking permissions, however no way to test this due to limitations
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Toast.makeText(this, "Connection Suspended. Retry again.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Toast.makeText(this, "Connection Failed.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        timeNotification();
        if(location == null){
            //Toast.makeText(this, "Cannot fetch current location.", Toast.LENGTH_LONG).show(); //for no  retrieved location due to various reasons(no service, gps off etc.)
        } else {

            LatLng ll = new LatLng(location.getLatitude(), location.getLongitude()); //gets the lat and long of current position from location requests

            //CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
            CameraPosition cameraPosition = new CameraPosition.Builder()    //updates the location
                                                            .target(ll)
                                                            .zoom(19)
                                                            .build();

            if(mCurrLocationMarker == null) {
                mCurrLocationMarker = mgoogleMap.addMarker(new MarkerOptions()
                        .position(ll)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                        .title("You")
                        .flat(true)
                        .zIndex(20));

                mgoogleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition), 2000, null); //animation to location over 2 secs




            } else {
                mCurrLocationMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude())); //sets the marker on the new position
                mgoogleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition), 2000, null);
            }


        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiverForAudioWalk);

        removeGeofences();

        if(player != null){
            if(player.isPlaying()){
                player.stop();
                player.release();
                player = null;
            }
        }

        if(player2 != null){
            if(player2.isPlaying()){
                player2.stop();
                player2.release();
                player2 = null;
            }
        }
        super.onDestroy();
    }

    private void createGeofences() throws ExecutionException, InterruptedException {

        //createGeofence(MYLATITUDE,MYLONGITUDE,0);
        //markerForGeofence(new LatLng(MYLATITUDE, MYLONGITUDE));

        //Toast.makeText(this, "Geofences have been added. Lat: " + MYLATITUDE + " Lng: " + MYLONGITUDE, Toast.LENGTH_SHORT).show();

        GetGeofencesData getGeofencesData = new GetGeofencesData(this);
        Cursor cursor = getGeofencesData.execute().get();
        geofencesData = cursor;//class variable for access

        cursor.moveToFirst();

        for(int i = 0; i < cursor.getCount(); i++){
            createGeofence(cursor.getDouble(0),cursor.getDouble(1), cursor.getInt(2));
            //markerForGeofence(new LatLng(cursor.getDouble(0),cursor.getDouble(1)));

            //Toast.makeText(this, "Geofences have been added. Lat: " + cursor.getDouble(0) + " Lng: " + cursor.getDouble(1), Toast.LENGTH_SHORT).show();
            cursor.moveToNext();
        }



    }

    private void createGeofence(double latitude, double longitude, int id){

        String requestId = id + "";

        mGeofenceList.add(new Geofence.Builder()
                .setRequestId(requestId)
                .setCircularRegion(latitude,longitude,RADIUS)
                .setExpirationDuration(NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT | Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(5000)
                .build());
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(forInitialTrigger);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.

        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }

        //Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);

        Intent intent = new Intent("GeofenceBroadcast");

        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
//        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
//

        mGeofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private Markers markerForGeofence(LatLng latLng) {

        // Define marker options
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        Marker geofenceMarker = mgoogleMap.addMarker(markerOptions);


        CircleOptions circleOptions = new CircleOptions()
                .center( geofenceMarker.getPosition())
                .strokeColor(Color.argb(50, 70,70,70))
                .fillColor( Color.argb(100, 150,150,150) )
                .radius(RADIUS);
        mgoogleMap.addCircle( circleOptions );

        return new Markers(circleOptions, markerOptions);
    }

    public void playTourSample(final int mediaResourceId){

        if(player2 != null){
            if(player2.isPlaying()){
                player2.stop();
                player2.release();
                player2 = null;
            }
        }

        player2 = MediaPlayer.create(getApplicationContext(), mediaResourceId);
        player2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                player2.release();
                player2 = null;
            }

        });

        player = MediaPlayer.create(getApplicationContext(), R.raw.aud_alert);
        player.start();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                player.release();
                player = null;
                player2.start();
            }
        });

    }

    public void playAudio(final int mediaResourceId){
        if(player2 != null){
            if(player2.isPlaying()){
                player2.stop();
                player2.release();
                player2 = null;
            }
        }

        player2 = MediaPlayer.create(getApplicationContext(), mediaResourceId);
        player2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                player2.release();
                player2 = null;
            }

        });
        player2.start();
    }

    @Override
    protected void onStop() {
        if(player2==null){
            super.onStop();
        }else{
            if(player2.isPlaying()){
                player2.stop();
                player2.release();
                player2 = null;
            }

            super.onStop();
        }


    }
    public void timeNotification(View view) {
        if (mtime != 7 || mtime != 12 || mtime != 16 || mtime != 19 ) {
            tShowed = false;}

        if (!tShowed) {
            Long aTime = new GregorianCalendar().getTimeInMillis() + 2 * 1000;

            Intent timeNotifIntent = new Intent(this, AlertReceiver.class);
            AlarmManager tAlManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            tAlManager.set(AlarmManager.RTC_WAKEUP, aTime, PendingIntent.getBroadcast(this, 18, timeNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT));
            tShowed = true;
            }
        }
    public void timeNotification() {
        if (mtime != 7 || mtime != 12 || mtime != 16 || mtime != 19 ) {
            tShowed = false;}

        if (!tShowed) {
            Long aTime = new GregorianCalendar().getTimeInMillis() + 2 * 1000;

            Intent timeNotifIntent = new Intent(this, AlertReceiver.class);
            AlarmManager tAlManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            tAlManager.set(AlarmManager.RTC_WAKEUP, aTime, PendingIntent.getBroadcast(this, 18, timeNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT));
            tShowed = true;
        }
    }

    private void removeGeofences(){
        mGeofencingClient.removeGeofences(getGeofencePendingIntent())
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Geofence", "Geofences have been removed.");
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Geofence", "Geofence removal failed..");
                    }
                });
    }

    private class Markers{

        CircleOptions circleOptions;
        MarkerOptions markerOptions;

        public Markers(CircleOptions circleOptions, MarkerOptions markerOptions){
            this.circleOptions = circleOptions;
            this.markerOptions = markerOptions;
        }

        public Markers getMarkers(){
            return new Markers(this.circleOptions, this.markerOptions);
        }

        public CircleOptions getCircleOptions(){
            return this.circleOptions;
        }

        public MarkerOptions getMarkerOptions(){
            return this.markerOptions;
        }
    }


    private void getCoordinates() {
        FusedLocationProviderClient mFusedLocationClient = getFusedLocationProviderClient(this);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {


                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

//                    Toast.makeText(SplashActivity.this, "Location fetch success.", Toast.LENGTH_SHORT).show();
//                    Toast.makeText(SplashActivity.this, "Lat: " + latitude + " Long: + " + longitude, Toast.LENGTH_SHORT).show();

                    latLongOfUser[0] = latitude;
                    latLongOfUser[1] = longitude;


                }
            }
        });
    }
}

