package com.jalm.dayo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import java.util.ArrayList;

public class EventsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        final ExpandableListView elv=(ExpandableListView) findViewById(R.id.expandableListView1);
        final ArrayList<eventsDetails> eventsDet = getDat();
        if (elv != null) {
            eventsAdapter adapter = new eventsAdapter(this, eventsDet);
            elv.setAdapter(adapter);

        }


        //create and bind to adapter
        eventsAdapter adptr = new eventsAdapter(this, eventsDet);
        elv.setPadding(0, 5, 0, 0);
        elv.setAdapter(adptr);

        elv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener(){
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id){
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });


    }



    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() -1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    //add/get data
    private ArrayList<eventsDetails> getDat()
    {
        eventsDetails e1= new eventsDetails("Niyogyugan Festival");
        e1.eventnames.add("\n     (Aug. 17 – Aug. 27). In 2011, Quezon Province held the first AGRI-TOURISM Trade Fair featuring booths adorned by agricultural produce and products from different municipalities here in Quezon. In 2012, the very first provincial festival aptly called, NIYOGYUGAN Festival, the celebration of the tree of life, was organized that was inspired by the Agri-Tourism Trade Fair. The Niyogyugan, which came from the words \"niyog\" and \"yugyog\" (to move to a beat), was conceptualized by former Congresswoman Aleta Suarez and there will be 3 inter-related events/competitions namely: Float, Dance, and Festival Queen.\n");

        eventsDetails e2 = new eventsDetails("Quezon Day");
        e2.eventnames.add("\n     Quezon Province declared August 19 a local holiday. This is to give respect to former President Manuel Luis Quezon. Also, on the same day, they hold a ceremony wherein they offer flowers to the Quezon Monument located in Perez Park. \n ");

        eventsDetails e3 = new eventsDetails("Pasayahan sa Lucena");
        e3.eventnames.add("\n     (May 24 – May 30). Pasayahan sa Lucena was conceptualized to showcase the natural and ecological interrelationship and independence between nature and man. It also promotes the ways of life inherent among the people of Lucena. All these find exquisites and appreciative expressions through a mammoth gathering of colors, outlandish costumes and symbolic floats reminiscent of Mardi Gras in Rio de Janeiro and New Orleans. Originally intended as three days of spirited merrymaking in the streets, the event has become a weeklong tourist attraction, culminating on May 30 in time for the celebration of the Feast of St. Ferdinand, the patron saint of Lucena.\n");

        eventsDetails e4 = new eventsDetails("Chami Festival");
        e4.eventnames.add("\n     Chami Festival that would feature Lucena's very own pansit delicacy. The traditional Chami Festival has a contest of who can cook the most delicious chami. The contestants line up along Quezon Avenue, the city’s main road, armed with their cooking utensils and will be provided free chami noodles, meat and other condiments for the cooking fest. All participants will be given cash incentive, gift packs from various sponsors. After the cooking, the spectators will be given a chance to eat for free the different taste of chami. They want that through this chami cooking festival this city will become a destination of our local and foreign tourists every merry month of May. \n");

        eventsDetails e5 = new eventsDetails("Araw ng Lucena");
        e5.eventnames.add("\n     Araw ng Lucena is the official day of Lucena. It is celebrated together with Niyogyugan Festival. In this festival, the locals showcase their specialties, talents and culture to people. This is celebrated every August 20.\n");

        eventsDetails e6 = new eventsDetails("Hermano Pule Day");
        e6.eventnames.add("\n     Lucena City declared November 4 a local holiday to give respect to Apolinario de la Cruz also known as Hermano Pule. Hermano Pule was a Filipino religious leader who founded and led the Cofradia de San Jose. It is a movement that was against racial discrimination of the Roman Catholic Church in the Philippines. \n");

        eventsDetails e7 = new eventsDetails("Hagisan ng Suman");
        e7.eventnames.add("\n     Hagisan ng Suman is originally a Tayabas festivity. However, Lucena City also participates in their festivity. It is celebrated in honor of St. Isidore Labrador wherein the locals will throw suman, a rice delicacy during the procession of St. Isidore Labrador’s statue. This is celebrated every May 15. \n");

        ArrayList<eventsDetails> allevents = new ArrayList<eventsDetails>();
        allevents.add(e1);
        allevents.add(e2);
        allevents.add(e3);
        allevents.add(e4);
        allevents.add(e5);
        allevents.add(e6);
        allevents.add(e7);

        return allevents;


    }

}
