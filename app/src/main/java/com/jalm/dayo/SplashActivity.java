package com.jalm.dayo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class SplashActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer = new MediaPlayer();

    private double[] latLongOfUser = new double[2];

    SharedPreferences SP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SP = PreferenceManager.getDefaultSharedPreferences(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getCoordinates();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    private void getCoordinates() {
        FusedLocationProviderClient mFusedLocationClient = getFusedLocationProviderClient(this);

        boolean showWelcomeMessage = SP.getBoolean("showWelcome", true);

        final Intent intent;

        if(showWelcomeMessage){
            intent = new Intent(SplashActivity.this, WelcomingMessageActivity.class);
        }else{
            intent = new Intent(SplashActivity.this, HomeActivity.class);
        }



        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {


                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    //Toast.makeText(SplashActivity.this, "Location fetch success.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(SplashActivity.this, "Lat: " + latitude + " Long: + " + longitude, Toast.LENGTH_SHORT).show();

                    latLongOfUser[0] = latitude;
                    latLongOfUser[1] = longitude;

                    intent.putExtra(HomeActivity.intentLatLongOfUser, latLongOfUser);
                    startActivity(intent);

                    finish();


                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this, R.style.AlertDialogTheme);
                    dialog.setTitle("Error");
                    dialog.setMessage("This application needs the Internet and the GPS in order to work. " +
                            "Please turn on your Data Connection and GPS Service. The application will now close.");
                    dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
                    dialog.show();
                }
            }
        });

        mFusedLocationClient.getLastLocation().addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this, R.style.AlertDialogTheme);
                dialog.setTitle("Error");
                dialog.setMessage("This application needs the Internet and the GPS in order to work. " +
                        "Please turn on your Data Connection and GPS Service. The application will now close.");
                dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getCoordinates();
    }
}
