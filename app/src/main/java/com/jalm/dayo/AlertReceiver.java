package com.jalm.dayo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.util.Calendar;

public class AlertReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Calendar calendar = Calendar.getInstance();
        int dayofmonth = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int time = calendar.get(Calendar.HOUR_OF_DAY);
        int tMinute = calendar.get(Calendar.MINUTE);


        switch (time) {
            case 7:
                if(tMinute >= 0 && tMinute <=1) {
                    timeNotification(context, "Breakfast time!", "You might want to check out Lucena City's food!");
                    break;
                }
            case 12:
                if(tMinute >= 0 && tMinute <=1) {
                    timeNotification(context, "Lunch Break!", "Check the nearby restaurants in our Food Category page.");
                    break;
                }
            case 16:
                if(tMinute >= 0 && tMinute <=1) {
                    timeNotification(context, "Treat yourself!", "There are a lot of place around Lucena City to have snacks!");
                    break;
                }
            case 19:
                if(tMinute >= 0 && tMinute <=1) {
                    timeNotification(context, "Time for Dinner!", "Enjoy your dinner around Lucena City. Check 'em out!");
                    break;
                }
        }

        switch (month) {
            case 7:
                if (dayofmonth == 17 && time == 8 || time == 13 && tMinute == 0){
                    eventsNotification(context, "Ongoing big event in Lucena City!", "Lucena City is celebrating Niyogyugan Festival!");
                    break;
                }
                else if (dayofmonth == 7 && time == 8 || time == 13 && tMinute == 0){
                    eventsNotification(context, "Today is Lucena City Day!", "Lucena City is celebrating Lucena Day!");
                    break;
                }
                else if (dayofmonth == 19 && time == 8 || time == 13 && tMinute == 0){
                    eventsNotification(context, "Happy Birthday Manuel L. Quezon!", "Lucena City is celebrating Quezon Day!");
                    break;
                }
                else {
                    break;
                }
            case 10:
                if (dayofmonth == 4 && time == 8 || time == 13 && tMinute == 0){
                    eventsNotification(context, "Today is a special day.", "Lucena City is giving respects to Hermano Puli, their local hero.");
                }
                else {
                    break;
                }
            case 4:
                if (dayofmonth ==  15 && time == 8 || time == 13 && tMinute == 0){
                    eventsNotification(context, "Suman Party!", "Lucena City is celebrating Hagisan ng Suman!");
                    break;
                }
                else if (dayofmonth == 26 && time == 8 || time == 13 && tMinute == 0){
                    eventsNotification(context, "Chami Galore!", "Lucena City is celebrating Chami Festival! YUMM!");
                    break;
                }
                else if (dayofmonth == 24 && time == 8 || time == 13 && tMinute == 0){
                    eventsNotification(context, "Pasayahan in Lucena!", "Lucena City is celebrating Pasayahan! Come and celebrate!");
                    break;
                }
                else {
                    break;
                }


        }
    }

    public void eventsNotification(Context context, String title, String message){

        PendingIntent eventsIntent = PendingIntent.getActivity(context, 17, new Intent(context, EventsActivity.class), 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo_small)
                .setContentTitle(title)
                .setTicker("Event in Lucena City")
                .setContentText(message)
                .setOnlyAlertOnce(true);

        mBuilder.setContentIntent(eventsIntent);
        mBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(1, mBuilder.build());


    }
    public void timeNotification(Context context, String tTitle, String tMessage){
        PendingIntent pi = PendingIntent.getActivity(context, 0, new Intent(context, PlaceCategoriesActivity.class), 0);

        NotificationCompat.Builder tBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo_small)
                .setContentTitle(tTitle)
                .setTicker("Meal Time")
                .setContentText(tMessage)
                .setOnlyAlertOnce(true);

        tBuilder.setContentIntent(pi);
        tBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
        tBuilder.setAutoCancel(true);
        NotificationManager tNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        tNotificationManager.notify(1, tBuilder.build());

    }


}
