package com.jalm.dayo;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jalm.dayo.R;

import java.util.ArrayList;

/**
 * Created by tranc on 10/09/2017.
 */

public class PopularPlacesAdapter extends CursorAdapter {

    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<Integer> listOf_id = new ArrayList<Integer>();

    public PopularPlacesAdapter(Context context, Cursor c) {
        super(context, c);
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

        c.moveToFirst();

        for(int i = 0; i < c.getCount(); i++){
            listOf_id.add(c.getInt(0));
            c.moveToNext();
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = layoutInflater.inflate(R.layout.popular_list_item, viewGroup, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        View aSingleView = view;

        //TextView placeName = (TextView)aSingleView.findViewById(R.id.place_name);
        //TextView placeDetails = (TextView)aSingleView.findViewById(R.id.place_details);


        //placeName.setText(cursor.getString(1));
        //placeDetails.setText(cursor.getString(2));

        ImageView imageViewPopularPlace = (ImageView)aSingleView.findViewById(R.id.imageViewPopularPlace);
        TextView tvNameOfPopularPlace = (TextView)aSingleView.findViewById(R.id.tvNameOfPopularPlace);

//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.RGB_565;
//        options.inSampleSize = 2;
//        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), cursor.getInt(1), options);
//
//        Palette singlePalette = createPaletteSync(bm);

        Glide.with(context).load(cursor.getInt(1)).into(imageViewPopularPlace);
        tvNameOfPopularPlace.setText(cursor.getString(2));

    }

    //this works with the long l parameter of the onItemClickListener, this is overriden
    //to get the _id of a single Adapter item
    @Override
    public long getItemId(int position) {
        return listOf_id.get(position);
    }

    public Palette createPaletteSync(Bitmap bitmap){
        Palette p = Palette.from(bitmap).generate();
        return p;


    }
}
