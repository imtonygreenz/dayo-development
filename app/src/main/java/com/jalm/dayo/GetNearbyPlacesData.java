package com.jalm.dayo;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tranc on 19/08/2017.
 * Does background gathering of nearby places
 */

public class GetNearbyPlacesData extends AsyncTask<Object, String, String>{

    String googlePlacesData;
    //GoogleMap googleMap;
    private String url;
    private Activity activity;
    private double[] latLongOfUser;

    public GetNearbyPlacesData(Activity passedActivity){
        this.activity = passedActivity;
    }

    @Override
    protected void onPostExecute(String s) {
        try{
            DataParser parser = new DataParser();
            List<HashMap<String,String>> places = parser.parse(s);

            Object object = places;

            Intent intent = new Intent(activity, ListOfPlacesByCategoryActivity.class);
            intent.putExtra(ListOfPlacesByCategoryActivity.intentLISTOFPLACES, (Serializable) places);
            intent.putExtra(PlaceDetailsActivity.intentUSERLATLONG, latLongOfUser);
            activity.startActivity(intent);
        }catch(NullPointerException e){
            e.printStackTrace();
            Handler handler = new Handler(activity.getMainLooper());
            handler.post(new Runnable(){

                @Override
                public void run() {
                    Toast.makeText(activity, "Not connected to the internet.", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }


    //when GetNearbyPlacesData.execute is invoked, this method is called
    @Override
    protected String doInBackground(Object... objects) {
        //googleMap = (GoogleMap)objects[0]; //passes the fragment and references the map from an activity
        url = (String)objects[0]; //passes a string object
        latLongOfUser = (double[])objects[1];

        DownloadURL downloadURL = new DownloadURL();
        try {
            googlePlacesData = downloadURL.readURL(url);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();

            Handler handler = new Handler(activity.getMainLooper());
            handler.post(new Runnable(){

                @Override
                public void run() {
                    Toast.makeText(activity, "Not connected to the internet.", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return googlePlacesData;
    }
}