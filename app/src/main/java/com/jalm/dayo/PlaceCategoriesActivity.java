package com.jalm.dayo;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.*;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

//TODO: Make user set the proximity radius
public class PlaceCategoriesActivity extends AppCompatActivity {

    //AIzaSyCYveG9Qow1kfrG4d12aq6hHhFTbqGb4-I Arvin key

    int PROXIMITY_RADIUS = 1000;

    private FusedLocationProviderClient mFusedLocationClient;
    private double latLongOfUser[] = new double[2];
    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        ListView lvMenu = (ListView) findViewById(R.id.lvMenu);
        ArrayList<String> placeCategories = new ArrayList<String>();



        for (PlaceCategories singlePlaceCategory : PlaceCategories.placeCategories) {
            placeCategories.add(singlePlaceCategory.getPlaceCategoriesName());
        }

        PlaceCategoriesAdapter placeCategoriesAdapter = new PlaceCategoriesAdapter(this,
                R.layout.category_list_item, PlaceCategories.placeCategories);

        if (googleServicesAvailable()) {
            //Toast.makeText(this, "Connected to Google Services.", Toast.LENGTH_SHORT).show();

            mFusedLocationClient = getFusedLocationProviderClient(this);

            if(mFusedLocationClient == null){
                //Toast.makeText(this, "FusedLocationClient is null", Toast.LENGTH_SHORT).show();
            }else{
                //Toast.makeText(this, "FusedLocationClient is available", Toast.LENGTH_SHORT).show();
            }

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

            //this registers mFusedLocation to a listener, the onSuccess method will be called once natapos na yung getLastLocation method
            // Register a listener, that will be called when the location will be available

            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location != null){


                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        //Toast.makeText(PlaceCategoriesActivity.this, "Location fetch success.", Toast.LENGTH_SHORT).show();
                        //Toast.makeText(PlaceCategoriesActivity.this, "Lat: " + latitude + " Long: + " + longitude, Toast.LENGTH_SHORT).show();

                        latLongOfUser[0] = latitude;
                        latLongOfUser[1] = longitude;
                    }else{
                        //Toast.makeText(PlaceCategoriesActivity.this, "Location fetch failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            //tapos nang iset up yung listener, and nainvoke na yung getLastLocation, we have to wait
            //for a several time before magtrigger ang listener at tawagin ang onSuccess method nito


            //Toast returns a value of 0 on latitude and 0 on longitude because, the mFusedLocationClient is still not
            //yet done with its Task, nagkamali ako, kasi akala ko, na after kong maset yung addOnSuccessListener ay
            //iinvoke na agad nito yung onSuccess method,
            //nalimutan ko na maiinvoke lamang yung onSuccess method pag natapos na yung Task method na getLastLocation();
            //the addOnSuccessListener will only work once na matapos yung Task event ni mFusedLocation

            lvMenu.setAdapter(placeCategoriesAdapter);
            lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    View aSingleListItem = view;

                    TextView tvCategoryName = (TextView)aSingleListItem.findViewById(R.id.tvCategoryName);

                    String categoryName = "" + tvCategoryName.getText();
                    categoryName = categoryName.toLowerCase();

                    GetNearbyPlacesData getNearbyPlacesData;
                    String url;

                    //TODO: prompt user if no places found near you
                    switch (categoryName) {
                        case "foods":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "restaurant");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "hospitals":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "hospital");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "government":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "local_government_office");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "schools":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "school");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "banks":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "bank");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "salons":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "beauty_salon");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "churches":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "church");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "stores":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "store");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "parks":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "park");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                        case "pharmacies":
                            getNearbyPlacesData = new GetNearbyPlacesData(PlaceCategoriesActivity.this);
                            url = getUrl(latitude, longitude, "pharmacy");
                            getNearbyPlacesData.execute(url, latLongOfUser);
                            break;
                    }
                }
            });
        }else{
            //Toast.makeText(this, "Failed to connect to Google Services.", Toast.LENGTH_SHORT).show();
        }
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace) {

//        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
//        googlePlaceUrl.append("location=" + latitude + "," + longitude);
//        googlePlaceUrl.append("&radius=" + PROXIMITY_RADIUS);
//        googlePlaceUrl.append("&type=" + nearbyPlace);
//        googlePlaceUrl.append("&sensor=true");
//        googlePlaceUrl.append("&key=" + "AIzaSyCYveG9Qow1kfrG4d12aq6hHhFTbqGb4-I");

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/textsearch/json?query=");
        googlePlaceUrl.append(nearbyPlace);
        googlePlaceUrl.append("+in+Lucena+City+Quezon");
        googlePlaceUrl.append("&location=" + latitude + "," + longitude);
        googlePlaceUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlaceUrl.append("&key=" + "AIzaSyCYveG9Qow1kfrG4d12aq6hHhFTbqGb4-I");

        return googlePlaceUrl.toString();
    }

    public boolean googleServicesAvailable() {
        // method checking if there is google services on the device or can connect to it

        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            //Toast.makeText(this, "Cannot connect to Google Play Services.", Toast.LENGTH_LONG).show();
        }
        return false;
    }
}

